import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthenticationService } from 'src/app/auth/authentication.service';
import { User } from 'src/app/auth/user.model';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  isNavbarCollapsed = true;
  user$: Observable<User>;

  constructor(private authenticationService: AuthenticationService) { }

  ngOnInit(): void {
    this.user$ = this.authenticationService.getUserFromLocalStorage()
  }

  logout(): void{
    this.authenticationService.logout();
    this.user$ = this.authenticationService.getUserFromLocalStorage()
  }

}
