import { TestBed } from '@angular/core/testing';
import { NavbarComponent } from './navbar.component';

describe('NavbarComponent', () => {
  let component: NavbarComponent;
  let authServiceSpy: any;
  
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NavbarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    authServiceSpy = jasmine.createSpyObj('AuthService', ['getUserFromLocalStorage']);
    component = new NavbarComponent(authServiceSpy)
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
