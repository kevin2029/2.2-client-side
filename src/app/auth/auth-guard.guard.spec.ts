import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { AlertService } from '../shared/alert';

import { AuthGuardGuard } from './auth-guard.guard';
import { AuthenticationService } from './authentication.service';

describe('AuthGuardGuard', () => {
  let guard: AuthGuardGuard;

  let authenticationServiceSpy;
  let routerSpy;
  let alertServiceSpy;

  beforeEach(() => {
    authenticationServiceSpy = jasmine.createSpyObj('AuthenticationService', [
      'userMayEdit',
      'getUserFromLocalStorage',
      'login',
    ]);
    routerSpy = jasmine.createSpyObj('Router', ['navigate']);
    alertServiceSpy = jasmine.createSpyObj('AlertService', ['succes', 'error']);

    guard = new AuthGuardGuard(
      authenticationServiceSpy,
      routerSpy,
      alertServiceSpy
    );
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
