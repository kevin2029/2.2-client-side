export class User {
    id: string;
    username:  string;    
    password:  string;
    email: string;
    birthdate: Date;
    role:  string;
}