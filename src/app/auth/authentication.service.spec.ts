import { TestBed } from '@angular/core/testing';

import { AuthenticationService } from './authentication.service';

describe('AuthenticationService', () => {
  let service: AuthenticationService;

  let routerSpy;
  let alertServiceSpy;
  let httpSpy: any;

  beforeEach(() => {
    httpSpy = jasmine.createSpyObj('HttpClient', ['get', 'post']);
    routerSpy = jasmine.createSpyObj('Router', ['navigate']);
    alertServiceSpy = jasmine.createSpyObj('AlertService', ['succes']);

    service = new AuthenticationService(httpSpy, alertServiceSpy, routerSpy);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
