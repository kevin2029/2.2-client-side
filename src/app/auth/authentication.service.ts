import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AlertService } from '../shared/alert/alert.service';
import { User } from './user.model';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  private authUrl = `${environment.api_url}/api`;
  public currentUser$ = new BehaviorSubject<User>(undefined);

  private readonly CURRENT_USER = 'currentuser';

  private readonly httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  alertOptions = {
    autoClose: true,
    keepAfterRouteChange: true,
  };

  constructor(
    private http: HttpClient,
    private alertService: AlertService,
    private router: Router
  ) {}

  login(username: string, password: string): Observable<User> {
    console.log(
      `login at ${this.authUrl}/authentication/login with ${username}, ${password}`
    );

    return this.http
      .post(
        `${this.authUrl}/authentication/login`,
        { username, password },
        this.httpOptions
      )
      .pipe(
        // tap(console.log),
        map((response: any) => {
          const user = { ...response } as User;
          this.saveUserToLocalStorage(user);
          this.currentUser$.next(user);
          this.currentUser$.isStopped = false;
          return user;
        }),
        catchError((error: any) => {
          console.log('error', error);
          return of(undefined);
        })
      );
  }
  register(
    username: string,
    password: string,
    email: string,
    birthdate: Date
  ): Observable<User> {
    return this.http
      .post(
        `${this.authUrl}/authentication/register`,
        { username, password, email, birthdate },
        this.httpOptions
      )
      .pipe(
        map((response: any) => {
          const user = { ...response } as User;
          this.saveUserToLocalStorage(user);
          this.currentUser$.next(user);
          this.alertService.success(
            'Gebruiker aangemaakt, u kunt nu inloggen.',
            this.alertOptions
          );
          return user;
        }),
        catchError((error: any) => {
          console.log('error', error);
          console.log('error.message:', error.message);
          console.log('error.error.message:', error.error.message);
          return of(undefined);
        })
      );
  }

  logout(): void {
    localStorage.removeItem(this.CURRENT_USER);
    console.log(this.currentUser$);
    this.currentUser$.isStopped = true;
    console.log(this.currentUser$);
    this.router.navigate(['/']);
  }

  getUserFromLocalStorage(): Observable<User> {
    if (this.currentUser$.isStopped === true) {
      return of(undefined);
    } else {
      const localUser = JSON.parse(localStorage.getItem(this.CURRENT_USER));
      return of(localUser);
    }
  }
  userMayEdit(itemUserId: string): Observable<boolean> {
    return this.currentUser$.pipe(
      map((user: User) => (user ? user.id === itemUserId : false))
    );
  }

  checkAdmin(): Observable<boolean> {
    return this.currentUser$.pipe(
      map((user: User) => (user ? user.role === 'admin' : false))
    );
  }

  private saveUserToLocalStorage(user: User): void {
    localStorage.setItem(this.CURRENT_USER, JSON.stringify(user));
  }

  getToken(): string {
    if (localStorage.getItem(this.CURRENT_USER) != null) {
      const token = JSON.parse(localStorage.getItem(this.CURRENT_USER)).token;
      return token;
    } else {
      return 'none';
    }
  }

  getRole(): string {
    const role = JSON.parse(localStorage.getItem(this.CURRENT_USER)).role;
    return role;
  }
}
