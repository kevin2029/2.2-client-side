import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/shared/alert';
import { AuthenticationService } from '../authentication.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['../login/login.component.css'],
})
export class RegisterComponent implements OnInit {
  registerForm = new FormGroup({
    username: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
    passwordControl: new FormControl('', Validators.required),
    email: new FormControl('', [Validators.required, Validators.email]),
    birthdate: new FormControl('', Validators.required),
  });

  options = {
    autoClose: true,
    keepAfterRouteChange: false,
  };

  constructor(
    private router: Router,
    private alertService: AlertService,
    private authenticationService: AuthenticationService
  ) {}

  ngOnInit(): void {}

  onSubmit(): void {
    const password = this.registerForm.value.password;
    const passwordControl = this.registerForm.value.passwordControl;
    const username = this.registerForm.value.username;
    const email = this.registerForm.value.email;
    const birthdate = this.registerForm.value.birthdate;

    if (password == passwordControl) {
      this.authenticationService
        .register(username, password, email, birthdate)
        .subscribe((user) => {
          if (user) {
            console.log('user registered as ', user);
            this.router.navigate(['../login']);
          } else {
            this.alertService.error(
              'Gebruikersnaam is al in gebruik.',
              this.options
            );
          }
        });
    } else {
      this.alertService.error('Wachtwoord is niet gelijk!', this.options);
    }
  }
}
