import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/shared/alert';
import { AuthenticationService } from '../authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  loginForm = new FormGroup({
    username: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
  });

  failOptions = {
    autoClose: true,
    keepAfterRouteChange: false,
  };

  successOptions = {
    autoClose: true,
    keepAfterRouteChange: true,
  };
  constructor(
    private router: Router,
    private alertService: AlertService,
    private authenticationService: AuthenticationService
  ) {}

  ngOnInit(): void {}

  onSubmit(): void {
    const username = this.loginForm.value.username;
    const password = this.loginForm.value.password;

    this.authenticationService.login(username, password).subscribe((user) => {
      console.log(user);
      if (user) {
        console.log('user = ', user);
        this.alertService.success('Je bent ingelogd.', this.successOptions);
        this.router.navigate(['/home']);
      } else {
        this.alertService.error(
          'Gebruikersnaam of wachtwoord is onjuist.',
          this.failOptions
        );
      }
    });
  }
}
