import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router,
} from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AlertService } from '../shared/alert';
import { AuthenticationService } from './authentication.service';
import { User } from './user.model';

@Injectable({
  providedIn: 'root',
})
export class AuthRoleGuard implements CanActivate {
  Options = {
    autoClose: true,
    keepAfterRouteChange: true,
  };

  constructor(
    private authService: AuthenticationService,
    private router: Router,
    private alertService: AlertService
  ) {}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    return this.authService.currentUser$.pipe(
      map((user: User) => {
        if (user && this.authService.getRole() == 'admin') {
          return true;
        } else {
          console.log(this.authService.getRole());
          this.alertService.error('Niet Geautoriseerd.', this.Options);
          this.router.navigate(['home']);
          return false;
        }
      })
    );
  }
}
