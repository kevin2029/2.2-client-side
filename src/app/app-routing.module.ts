import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { LayoutComponent } from './layout/layout/layout.component';
import { UsecasesComponent } from './pages/about/usecases/usecases.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';

const routes: Routes = [
  { path: 'login', pathMatch: 'full', component: LoginComponent },
  { path: 'register', pathMatch: 'full', component: RegisterComponent },
  {
    path: '',
    component: LayoutComponent,
    children: [
      { path: '', pathMatch: 'full', component: DashboardComponent },
      { path: 'home', pathMatch: 'full', component: DashboardComponent },
      { path: 'about', pathMatch: 'full', component: UsecasesComponent },
      {
        path: 'games',
        loadChildren: () =>
          import('./pages/game/game.module').then((g) => g.GameModule),
      },
      {
        path: 'characters',
        loadChildren: () =>
          import('./pages/character/character.module').then(
            (c) => c.CharacterModule
          ),
      },
      {
        path: 'personalLists',
        loadChildren: () =>
          import('./pages/personalList/personalList.module').then(
            (p) => p.PersonalListModule
          ),
      },
    ],
  },

  { path: '**', pathMatch: 'full', redirectTo: 'home' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
