import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import * as fromComponents from './index';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthGuardGuard } from 'src/app/auth/auth-guard.guard';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: fromComponents.PersonalListComponent,
  },
  {
    path: ':id',
    pathMatch: 'full',
    component: fromComponents.PersonalDetailComponent,
  },
];
@NgModule({
  declarations: [...fromComponents.components],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
  ],
})
export class PersonalListModule {}
