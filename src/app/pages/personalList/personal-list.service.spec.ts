import { of, throwError } from 'rxjs';
import { PersonalListService } from './personal-list.service';
import { PersonalList } from './personalList.model';

const userId = 'TestId1';
const expectedPersonalList: PersonalList[] = [
  {
    id: 'TestId',
    userId: 'TestId',
    name: 'Test',
    games: [],
    dateCreated: 'Test',
  },
  {
    id: 'TestId2',
    userId: 'TestId2',
    name: 'Test2',
    games: [],
    dateCreated: 'Test2',
  },
];

const expectedOnePersonalList: PersonalList = {
  id: 'TestId',
  userId: 'TestId',
  name: 'listName',
  games: [],
  dateCreated: 'Test',
};

const expectedErrorResponse = {
  error: { message: 'not found!' },
  name: 'HttpErrorResponse',
  ok: false,
  status: 401,
};

describe('PersonalListService', () => {
  let service: PersonalListService;

  //mock
  let httpSpy: any;
  let authServiceSpy: any;

  beforeEach(() => {
    authServiceSpy = jasmine.createSpyObj('AuthService', ['getToken']);
    httpSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'delete']);

    service = new PersonalListService(authServiceSpy, httpSpy);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('getAllByUserId() should return all the lists for one user with valid values', () => {
    httpSpy.get.and.returnValue(of(expectedPersonalList));

    const subscription = service.getAllByUserId(userId).subscribe((list) => {
      expect(list.length).toEqual(2);
      expect(list[0].name).toEqual('Test');
    });

    subscription.unsubscribe();
  });

  it('getAllByUserId() should return all the lists for one user with NOT valid values', () => {
    const userId = 'TestId1';
    httpSpy.get.and.returnValue(throwError(expectedErrorResponse));

    const subscription = service.getAllByUserId(userId).subscribe((error) => {
      expect(error).toEqual([]);
      expect(error.length).toEqual(0);
    });

    subscription.unsubscribe();
  });

  it('create() should create a personallist and return the list with valid values', () => {
    const name = 'listName';
    httpSpy.post.and.returnValue(of(expectedOnePersonalList));

    const subscription = service.create(name, userId).subscribe((list) => {
      expect(list.name).toEqual(name);
    });

    subscription.unsubscribe();
  });

  it('create() should NOT create a personallist and return user not found', () => {
    const name = 'listName';
    httpSpy.post.and.returnValue(throwError(expectedErrorResponse));

    const subscription = service.create(name, userId).subscribe((error) => {
      expect(error).toEqual(undefined);
    });

    subscription.unsubscribe();
  });

  it('delete() should delete list when valid id is given', () => {
    httpSpy.delete.and.returnValue(of(expectedOnePersonalList));

    const subscription = service.delete(userId).subscribe((list) => {
      expect(list).toEqual(jasmine.objectContaining(expectedOnePersonalList));
      expect(list.name).toEqual('listName');
    });

    subscription.unsubscribe();
  });

  it('delete() should throw error when non valid id is given', () => {
    httpSpy.delete.and.returnValue(throwError(expectedErrorResponse));

    const subscription = service.delete(userId).subscribe((error) => {
      expect(error).toEqual(undefined);
    });

    subscription.unsubscribe();
  });

  it('deleteFromList() should delete a game from the list when valid values are given', () => {
    httpSpy.delete.and.returnValue(of(expectedOnePersonalList));
    const gameId = 'gameTestId';

    const subscription = service
      .deleteFromList(userId, gameId)
      .subscribe((list) => {
        expect(list.userId).toEqual('TestId');
      });

    subscription.unsubscribe();
  });

  it('deleteFromList() should not delete game when wrong values are given', () => {
    httpSpy.delete.and.returnValue(throwError(expectedErrorResponse));
    const gameId = 'gameTestId';

    const subscription = service
      .deleteFromList(userId, gameId)
      .subscribe((error) => {
        expect(error).toEqual(undefined);
      });

    subscription.unsubscribe();
  });
});
