import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/auth/authentication.service';
import { environment } from 'src/environments/environment';
import { PersonalList } from './personalList.model';

@Injectable({
  providedIn: 'root',
})
export class PersonalListService {
  private personalListURL = `${environment.api_url}/api/personalList`;

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: this.authService.getToken(),
    }),
  };
  constructor(
    private authService: AuthenticationService,
    private http: HttpClient
  ) {}

  getAllByUserId(userId: string): Observable<PersonalList[]> {
    return this.http
      .get<PersonalList[]>(`${this.personalListURL}/${userId}`)
      .pipe(
        tap((_) => console.log('get all List for user')),
        catchError(this.handleError<PersonalList[]>('Get all', []))
      );
  }

  getOneByListId(listId: string): Observable<PersonalList> {
    return this.http
      .get<PersonalList>(`${this.personalListURL}/one/${listId}`)
      .pipe(
        tap((_) => console.log('get all List for user')),
        catchError(this.handleError<PersonalList>('Get all'))
      );
  }

  create(name: string, userId: string): Observable<PersonalList> {
    return this.http
      .post<PersonalList>(
        `${this.personalListURL}/create`,
        { name, userId },
        this.httpOptions
      )
      .pipe(
        tap((createdGame: PersonalList) =>
          console.log(`Create Game: ${createdGame.id}`)
        ),
        catchError(this.handleError<PersonalList>('create personallist'))
      );
  }

  delete(id: string): Observable<PersonalList> {
    const url = `${this.personalListURL}/${id}`;
    return this.http.delete<PersonalList>(url, this.httpOptions).pipe(
      tap((_) => console.log(`deleted List: ${id}`)),
      catchError(this.handleError<PersonalList>('delete List'))
    );
  }

  deleteFromList(listId: string, gameId: string): Observable<PersonalList> {
    const url = `${this.personalListURL}/${listId}/delete/game/${gameId}`;
    return this.http.delete<PersonalList>(url, this.httpOptions).pipe(
      tap((_) => console.log(`deleted from List: ${gameId}`)),
      catchError(this.handleError<PersonalList>('delete List'))
    );
  }

  handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      return of(result);
    };
  }
}
