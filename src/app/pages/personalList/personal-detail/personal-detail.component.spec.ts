import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { Router, ActivatedRoute, convertToParamMap } from '@angular/router';
import { of, Subscription } from 'rxjs';
import { AuthenticationService } from 'src/app/auth/authentication.service';
import { AlertService } from 'src/app/shared/alert';
import { PersonalListService } from '../personal-list.service';
import { PersonalListComponent } from '../personal-list/personal-list.component';
import { PersonalList } from '../personalList.model';

import { PersonalDetailComponent } from './personal-detail.component';

describe('PersonalDetailComponent', () => {
  let component: PersonalDetailComponent;
  let fixture: ComponentFixture<PersonalDetailComponent>;

  let personalListServiceSpy;
  let authenticationServiceSpy;
  let alertServiceSpy;
  let routerSpy;

  const expectedPersonalList: PersonalList = {
    id: 'TestId',
    userId: 'TestId',
    name: 'Test',
    games: [],
    dateCreated: 'Test',
  };

  beforeEach(() => {
    personalListServiceSpy = jasmine.createSpyObj('PersonalListService', [
      'getOneByListId',
      'delete',
      'deleteFromList',
    ]);
    authenticationServiceSpy = jasmine.createSpyObj('AuthenticationService', [
      'userMayEdit',
      'getUserFromLocalStorage',
    ]);
    routerSpy = jasmine.createSpyObj('Router', ['navigate']);
    alertServiceSpy = jasmine.createSpyObj('AlertService', ['succes']);

    TestBed.configureTestingModule({
      declarations: [PersonalDetailComponent],
      imports: [FormsModule],
      providers: [
        { provide: PersonalListService, useValue: personalListServiceSpy },
        {
          provide: AuthenticationService,
          useValue: authenticationServiceSpy,
        },
        { provide: AlertService, useValue: alertServiceSpy },
        { provide: Router, useValue: routerSpy },
        {
          provide: ActivatedRoute,
          useValue: {
            paramMap: of(
              convertToParamMap({
                id: 'TestId',
              })
            ),
          },
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(PersonalDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => {
    fixture.destroy();
  });

  it('should create', () => {
    personalListServiceSpy.getOneByListId.and.returnValue(
      of(expectedPersonalList)
    );
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });
});
