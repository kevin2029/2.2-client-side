import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/auth/authentication.service';
import { AlertService } from 'src/app/shared/alert';
import { PersonalListService } from '../personal-list.service';
import { PersonalList } from '../personalList.model';

@Component({
  selector: 'app-personal-detail',
  templateUrl: './personal-detail.component.html',
  styleUrls: ['./personal-detail.component.css'],
})
export class PersonalDetailComponent implements OnInit {
  personalList$: Observable<PersonalList>;
  subscription: Subscription;

  options = {
    autoClose: true,
    keepAfterRouteChange: true,
  };

  constructor(
    private personalListService: PersonalListService,
    private authenticationService: AuthenticationService,
    protected alertService: AlertService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.personalList$ = this.route.paramMap.pipe(
      switchMap((params: ParamMap) =>
        this.personalListService.getOneByListId(params.get('id'))
      )
    );
  }

  delete(id: string): void {
    if (confirm('Weet je zeker dat je deze lijst wilt verwijderen?')) {
      this.subscription = this.personalListService
        .delete(id)
        .subscribe((_) =>
          this.router.navigate(['..'], { relativeTo: this.route })
        );
    }
  }

  deleteList(gameId: string, listId: string): void {
    if (confirm('Weet je zeker dat je deze game wilt verwijderen?')) {
      console.log('deleting game from list: ', listId);
      this.subscription = this.personalListService
        .deleteFromList(listId, gameId)
        .subscribe((_) => {
          this.alertService.success(
            'Game is verwijderd van de lijst!',
            this.options
          );
          this.router.navigate(['..'], { relativeTo: this.route });
        });
    }
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
