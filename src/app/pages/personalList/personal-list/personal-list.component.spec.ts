import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { Router, ActivatedRoute, convertToParamMap } from '@angular/router';
import { BehaviorSubject, of, Subscription } from 'rxjs';
import { AuthenticationService } from 'src/app/auth/authentication.service';
import { User } from 'src/app/auth/user.model';
import { AlertService } from 'src/app/shared/alert';
import { PersonalListService } from '../personal-list.service';
import { PersonalList } from '../personalList.model';

import { PersonalListComponent } from './personal-list.component';

const expectedPersonalList: PersonalList[] = [
  {
    id: 'TestId',
    userId: 'TestId',
    name: 'Test',
    games: [],
    dateCreated: 'Test',
  },
];

const expectedUserData: User = {
  id: 'TestId',
  username: 'Firstname',
  password: 'Lastname',
  email: 'user@host.com',
  birthdate: new Date(),
  role: 'test',
};

describe('PersonalListComponent', () => {
  let component: PersonalListComponent;
  let fixture: ComponentFixture<PersonalListComponent>;

  let personalListServiceSpy;
  let authenticationServiceSpy;
  let alertServiceSpy;

  beforeEach(() => {
    personalListServiceSpy = jasmine.createSpyObj('PersonalListService', [
      'getAllByUserId',
      'delete',
      'deleteFromList',
    ]);
    authenticationServiceSpy = jasmine.createSpyObj('authenticationService', [
      'userMayEdit',
      'getUserFromLocalStorage',
    ]);
    alertServiceSpy = jasmine.createSpyObj('AlertService', ['succes']);

    const mockUser$ = new BehaviorSubject<User>(expectedUserData);
    authenticationServiceSpy.currentUser$ = mockUser$;

    TestBed.configureTestingModule({
      declarations: [PersonalListComponent],
      imports: [FormsModule],
      providers: [
        { provide: PersonalListService, useValue: personalListServiceSpy },
        {
          provide: AuthenticationService,
          useValue: authenticationServiceSpy,
        },
        { provide: AlertService, useValue: alertServiceSpy },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(PersonalListComponent);
    component = fixture.componentInstance;
  });

  afterEach(() => {
    fixture.destroy();
  });

  it('should create', () => {
    authenticationServiceSpy.getUserFromLocalStorage.and.returnValue(
      of(expectedUserData)
    );

    personalListServiceSpy.getAllByUserId.and.returnValue(
      of(expectedPersonalList)
    );
    component.subscription = new Subscription();
    fixture.detectChanges();

    expect(component).toBeTruthy();
  });
});
