import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { AuthenticationService } from 'src/app/auth/authentication.service';
import { User } from 'src/app/auth/user.model';
import { AlertService } from 'src/app/shared/alert';
import { PersonalListService } from '../personal-list.service';
import { PersonalList } from '../personalList.model';

@Component({
  selector: 'app-personal-list',
  templateUrl: './personal-list.component.html',
  styleUrls: ['./personal-list.component.css'],
})
export class PersonalListComponent implements OnInit {
  create$: boolean;
  userId: string;
  personalLists$: Observable<PersonalList[]>;
  subscription: Subscription;

  options = {
    autoClose: true,
    keepAfterRouteChange: false,
  };

  personalListForm = new FormGroup({
    name: new FormControl(''),
  });

  constructor(
    private personalListService: PersonalListService,
    private authenticationService: AuthenticationService,
    protected alertService: AlertService
  ) {}

  ngOnInit(): void {
    this.create$ = false;

    this.subscription = this.authenticationService
      .getUserFromLocalStorage()
      .subscribe((user) => {
        this.userId = user.id;
      });

    this.personalLists$ = this.personalListService.getAllByUserId(this.userId);
  }

  onSubmit(): void {
    const nameList = this.personalListForm.value.name;

    this.personalListService.create(nameList, this.userId).subscribe((list) => {
      if (list) {
        this.alertService.success('Lijst is aangemaakt!', this.options);
        this.ngOnDestroy();
        this.ngOnInit();
      } else {
        this.alertService.error(
          'De naam van uw lijst is al in gebruik!',
          this.options
        );
      }
    });
  }

  create(): void {
    this.create$ = !this.create$;
  }

  ngOnDestroy(): void {
    this.subscription?.unsubscribe();
  }
}
