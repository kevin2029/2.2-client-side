import { PersonalDetailComponent } from './personal-detail/personal-detail.component';
import { PersonalListComponent } from './personal-list/personal-list.component';

export const components: any[] = [
  PersonalListComponent,
  PersonalDetailComponent,
];

export * from './personal-list/personal-list.component';
export * from './personal-detail/personal-detail.component';
