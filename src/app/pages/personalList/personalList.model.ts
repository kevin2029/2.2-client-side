export class PersonalList{
    id: string;
    userId: string;
    name: string;
    games: any[];
    dateCreated: string;
}