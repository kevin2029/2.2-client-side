import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AuthenticationService } from 'src/app/auth/authentication.service';
import { GameService } from '../game.service';

import { GameListComponent } from './game-list.component';

describe('GameListComponent', () => {
  let component: GameListComponent;
  let fixture: ComponentFixture<GameListComponent>;

  let gameServiceSpy;
  let authenticationServiceSpy;

  beforeEach(() => {
    gameServiceSpy = jasmine.createSpyObj('GameService', ['getAll']);
    authenticationServiceSpy = jasmine.createSpyObj('AuthenticationService', [
      'userMayEdit',
      'getUserFromLocalStorage',
    ]);

    TestBed.configureTestingModule({
      declarations: [GameListComponent],
      providers: [
        { provide: GameService, useValue: gameServiceSpy },
        { provide: AuthenticationService, useValue: authenticationServiceSpy },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(GameListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => {
    fixture.destroy();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
