import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthenticationService } from 'src/app/auth/authentication.service';
import { User } from 'src/app/auth/user.model';
import { Game } from '../game.model';
import { GameService } from '../game.service';

@Component({
  selector: 'app-Game-list',
  templateUrl: './game-list.component.html',
  styleUrls: ['./game-list.component.css']
})
export class GameListComponent implements OnInit {

  user$: Observable<User>;
  games$ : Observable<Game[]>;

  constructor(
    private GameService: GameService,
    private authenticationService: AuthenticationService
    ) { }

  ngOnInit(): void {
    console.log("Game list Loaded")
    this.user$ = this.authenticationService.getUserFromLocalStorage();
    this.games$ = this.GameService.getAll();
  }
}
