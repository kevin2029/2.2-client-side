import { GameService } from './game.service';

describe('GameService', () => {
  let service: GameService;

  //mock
  let httpSpy: any;
  let authServiceSpy: any;

  beforeEach(() => {
    authServiceSpy = jasmine.createSpyObj('AuthService', ['getToken']);
    httpSpy = jasmine.createSpyObj('HttpClient', ['get', 'post']);

    service = new GameService(httpSpy, authServiceSpy)
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
