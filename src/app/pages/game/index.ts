import { GameDetailComponent } from './game-detail/game-detail.component';
import { GameListComponent } from './game-list/game-list.component';
import { GameUpdateComponent } from './game-update/game-update.component';

export const components: any[] = [GameDetailComponent, GameListComponent, GameUpdateComponent];

export * from './game-detail/game-detail.component';
export * from './game-list/game-list.component';
export * from './game-update/game-update.component';

