import {
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick,
} from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute, convertToParamMap, Router } from '@angular/router';
import { BehaviorSubject, of, Subscription } from 'rxjs';
import { AuthenticationService } from 'src/app/auth/authentication.service';
import { User } from 'src/app/auth/user.model';
import { AlertService } from 'src/app/shared/alert';
import { Character } from '../../character/character.model';
import { CharacterService } from '../../character/character.service';
import { PersonalListService } from '../../personalList/personal-list.service';
import { PersonalList } from '../../personalList/personalList.model';
import { Game } from '../game.model';
import { GameService } from '../game.service';

import { GameDetailComponent } from './game-detail.component';

const expectedUserData: User = {
  id: 'TestId',
  username: 'Firstname',
  password: 'Lastname',
  email: 'user@host.com',
  birthdate: new Date(),
  role: 'test',
};

const expectedGame: Game = {
  id: 'TestId',
  title: 'TestTiltle',
  publisher: {
    name: 'test',
    founder: 'test',
    headquarterAdres: 'Kevin 23',
  },
  description: 'Test',
  releaseYear: 2000,
  characters: [],
  creatorId: 'Test',
};

const expectedPersonalList: PersonalList[] = [
  {
    id: 'TestId',
    userId: 'TestId',
    name: 'Test',
    games: [],
    dateCreated: 'Test',
  },
];

const expectedCharacterData: Character[] = [
  {
    id: 'TestId',
    name: 'Test',
    description: 'Test',
    firstAppearance: 'Test',
    gender: 'Test',
    creatorId: 'Test',
  },
  {
    id: 'TestId',
    name: 'Test',
    description: 'Test',
    firstAppearance: 'Test',
    gender: 'Test',
    creatorId: 'Test',
  },
];

describe('GameDetailComponent', () => {
  let component: GameDetailComponent;
  let fixture: ComponentFixture<GameDetailComponent>;

  let gameServiceSpy;
  let routerSpy;
  let authenticationServiceSpy;
  let alertServiceSpy;
  let personalListServiceSpy;
  let characterServiceSpy;

  beforeEach(() => {
    authenticationServiceSpy = jasmine.createSpyObj('authenticationService', [
      'userMayEdit',
      'getUserFromLocalStorage',
      'checkAdmin',
    ]);
    characterServiceSpy = jasmine.createSpyObj('CharacterService', ['getAll']);
    personalListServiceSpy = jasmine.createSpyObj('PersonalListService', [
      'getAllByUserId',
    ]);
    alertServiceSpy = jasmine.createSpyObj('AlertService', ['succes', 'error']);
    routerSpy = jasmine.createSpyObj('Router', ['navigate']);
    gameServiceSpy = jasmine.createSpyObj('GameService', [
      'getById',
      'addToList',
      'addToGame',
      'delete',
    ]);

    const mockUser$ = new BehaviorSubject<User>(expectedUserData);
    authenticationServiceSpy.currentUser$ = mockUser$;

    TestBed.configureTestingModule({
      declarations: [GameDetailComponent],
      imports: [FormsModule],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            paramMap: of(
              convertToParamMap({
                id: 'GameId',
              })
            ),
          },
        },
        { provide: GameService, useValue: gameServiceSpy },
        { provide: Router, useValue: routerSpy },
        {
          provide: AuthenticationService,
          useValue: authenticationServiceSpy,
        },
        { provide: AlertService, useValue: alertServiceSpy },
        { provide: PersonalListService, useValue: personalListServiceSpy },
        { provide: CharacterService, useValue: characterServiceSpy },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(GameDetailComponent);
    component = fixture.componentInstance;
  });
  afterEach(() => {
    fixture.destroy();
  });

  it('should create', () => {
    authenticationServiceSpy.getUserFromLocalStorage.and.returnValue(
      of(expectedUserData)
    );
    gameServiceSpy.getById.and.returnValue(of(expectedGame));

    personalListServiceSpy.getAllByUserId.and.returnValue(
      of(expectedPersonalList)
    );

    characterServiceSpy.getAll.and.returnValue(of(expectedCharacterData));

    component.subscriptionUser = new Subscription();
    fixture.detectChanges();

    expect(component).toBeTruthy();
  });
});
