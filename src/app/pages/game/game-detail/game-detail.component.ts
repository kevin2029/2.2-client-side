import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/auth/authentication.service';
import { User } from 'src/app/auth/user.model';
import { AlertService } from 'src/app/shared/alert';
import { Character } from '../../character/character.model';
import { CharacterService } from '../../character/character.service';
import { PersonalListService } from '../../personalList/personal-list.service';
import { PersonalList } from '../../personalList/personalList.model';
import { Game } from '../game.model';
import { GameService } from '../game.service';

@Component({
  selector: 'app-game-detail',
  templateUrl: './game-detail.component.html',
  styleUrls: ['./game-detail.component.css'],
})
export class GameDetailComponent implements OnInit, OnDestroy {
  addList$: boolean;
  addCharacter$: boolean;
  game$: Observable<Game>;
  characters$: Observable<Character[]>;
  personalLists$: Observable<PersonalList[]>;
  userId: string;
  subscription: Subscription;
  subscriptionUser: Subscription;

  personalListForm = new FormGroup({
    listId: new FormControl(''),
  });

  options = {
    autoClose: true,
    keepAfterRouteChange: false,
  };

  constructor(
    private gameService: GameService,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private alertService: AlertService,
    private personalListService: PersonalListService,
    private characterService: CharacterService
  ) {}

  ngOnInit(): void {
    //loads the game
    this.game$ = this.route.paramMap.pipe(
      tap((params: ParamMap) => console.log('game.id = ', params.get('id'))),
      switchMap((params: ParamMap) =>
        this.gameService.getById(params.get('id'))
      ),
      tap(console.log)
    );

    //get userId
    this.subscriptionUser = this.authenticationService
      .getUserFromLocalStorage()
      .subscribe((user) => {
        this.userId = user.id;
      });

    //get userlists
    this.personalLists$ = this.personalListService.getAllByUserId(this.userId);

    //get characters
    this.characters$ = this.characterService.getAll();
    this.addList$ = false;
    this.addCharacter$ = false;
  }

  onSubmit(gameId: string): void {
    const listId = (document.getElementById('listId') as HTMLInputElement)
      .value;

    this.gameService.addToList(gameId, listId).subscribe((item) => {
      if (item) {
        this.responseHandler('succes', 'list');
      } else {
        this.responseHandler('error', 'list');
      }
    });
  }

  onSubmitCharacter(gameId: string): void {
    const characterId = (document.getElementById(
      'characterId'
    ) as HTMLInputElement).value;

    this.gameService.addToGame(gameId, characterId).subscribe((item) => {
      console.log('Item:', item);
      if (item) {
        this.responseHandler('succes', 'game');
      } else {
        this.responseHandler('error', 'game');
      }
    });
  }

  delete(id: string, characterId: string, type: string): void {
    if (confirm('Weet je zeker dat je het wilt verwijderen?')) {
      if (type == 'game') {
        this.subscription = this.gameService
          .delete(id, '', 'game')
          .subscribe((_) =>
            this.router.navigate(['..'], { relativeTo: this.route })
          );
      }
      if (type == 'character') {
        this.subscription = this.gameService
          .delete(id, characterId, 'character')
          .subscribe((_) =>
            this.router.navigate(['..'], { relativeTo: this.route })
          );
      }
    }
  }

  addList(): void {
    this.addList$ = !this.addList$;
  }

  addCharacter(): void {
    this.addCharacter$ = !this.addCharacter$;
  }

  responseHandler(status: string, method: string) {
    if (status == 'succes') {
      this.alertService.success('Succes!', this.options);
      this.ngOnDestroy();
      this.ngOnInit();
    }

    if (status == 'error') {
      if (method == 'list') {
        this.alertService.error(
          'De naam van uw lijst is al in gebruik!',
          this.options
        );
      }
      if (method == 'game') {
        this.alertService.error(
          'de character zit al in de game!',
          this.options
        );
      }
    }
  }
  ngOnDestroy(): void {
    this.subscription?.unsubscribe();
    this.subscriptionUser?.unsubscribe();
  }
}
