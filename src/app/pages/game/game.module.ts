import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import * as fromComponents from './index';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthGuardGuard } from 'src/app/auth/auth-guard.guard';
import { AuthRoleGuard } from 'src/app/auth/auth-role.guard';

const routes: Routes = [
  { path: '', pathMatch: 'full', component: fromComponents.GameListComponent },
  {
    path: 'create',
    pathMatch: 'full',
    component: fromComponents.GameUpdateComponent,
    canActivate: [AuthRoleGuard, AuthGuardGuard],
  },
  {
    path: ':id',
    pathMatch: 'full',
    component: fromComponents.GameDetailComponent,
  },
  {
    path: ':id/update',
    pathMatch: 'full',
    component: fromComponents.GameUpdateComponent,
    //canActivate: [AuthRoleGuard, AuthGuardGuard],
  },
];
@NgModule({
  declarations: [...fromComponents.components],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
  ],
})
export class GameModule {}
