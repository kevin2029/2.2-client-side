export class Game {
  id: string;
  title: string;
  publisher: {
    name: string;
    founder: string;
    headquarterAdres: string;
  };
  description: string;
  releaseYear: number;
  characters: any[];
  creatorId: string;
}
