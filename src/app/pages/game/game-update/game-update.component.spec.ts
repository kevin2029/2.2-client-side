import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute, convertToParamMap, Router } from '@angular/router';
import { BehaviorSubject, of, Subscription, throwError } from 'rxjs';
import { AuthenticationService } from 'src/app/auth/authentication.service';
import { User } from 'src/app/auth/user.model';
import { AlertService } from 'src/app/shared/alert';
import { Game } from '../game.model';
import { GameService } from '../game.service';

import { GameUpdateComponent } from './game-update.component';

const expectedUserData: User = {
  id: 'TestId',
  username: 'Firstname',
  password: 'Lastname',
  email: 'user@host.com',
  birthdate: new Date(),
  role: 'test',
};

const expectedGame: Game = {
  id: 'TestId',
  title: 'TestTiltle',
  publisher: {
    name: 'test',
    founder: 'test',
    headquarterAdres: 'Kevin 23',
  },
  description: 'Test',
  releaseYear: 2000,
  characters: [],
  creatorId: 'Test',
};

const expectedErrorResponse = {
  error: { message: 'not found!' },
  name: 'HttpErrorResponse',
  ok: false,
  status: 401,
};

describe('GameUpdateComponent', () => {
  let component: GameUpdateComponent;
  let fixture: ComponentFixture<GameUpdateComponent>;

  let gameServiceSpy;
  let authenticationServiceSpy;
  let routerSpy;
  let alertServiceSpy;

  beforeEach(() => {
    authenticationServiceSpy = jasmine.createSpyObj('AuthenticationService', [
      'userMayEdit',
      'getUserFromLocalStorage',
    ]);
    routerSpy = jasmine.createSpyObj('Router', ['navigate']);
    alertServiceSpy = jasmine.createSpyObj('AlertService', [
      'success',
      'error',
    ]);

    gameServiceSpy = jasmine.createSpyObj('GameService', [
      'getById',
      'update',
      'create',
    ]);

    const mockUser$ = new BehaviorSubject<User>(expectedUserData);
    authenticationServiceSpy.currentUser$ = mockUser$;

    TestBed.configureTestingModule({
      declarations: [GameUpdateComponent],
      imports: [FormsModule],
      providers: [
        { provide: GameService, useValue: gameServiceSpy },
        {
          provide: AuthenticationService,
          useValue: authenticationServiceSpy,
        },
        { provide: AlertService, useValue: alertServiceSpy },
        { provide: Router, useValue: routerSpy },
        {
          provide: ActivatedRoute,
          useValue: {
            paramMap: of(
              convertToParamMap({
                id: 'TestId',
              })
            ),
          },
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(GameUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => {
    fixture.destroy();
  });

  it('should create', () => {
    gameServiceSpy.getById.and.returnValue(of(expectedGame));
    authenticationServiceSpy.getUserFromLocalStorage.and.returnValue(
      of(expectedUserData)
    );

    component.game = expectedGame;
    component.subscription = new Subscription();
    fixture.detectChanges();

    expect(component.game).toEqual(expectedGame);
    expect(component).toBeTruthy();
  });

  it('onSubmit with update game should be give succes message when valid info is given', () => {
    gameServiceSpy.update.and.returnValue(of(expectedGame));

    component.game = expectedGame;
    component.id = 'test';

    component.onSubmit('TestId');
    fixture.detectChanges();

    expect(alertServiceSpy.success.calls.count()).toBe(1);
    expect(routerSpy.navigate.calls.count()).toBe(1);
  });

  it('onSubmit with update game should be give error message when NON valid info is given', () => {
    component.game = expectedGame;

    gameServiceSpy.update.and.returnValue(throwError(expectedErrorResponse));

    component.onSubmit('NonExistentId');
    fixture.detectChanges();

    expect(alertServiceSpy.error.calls.count()).toBe(1);
    expect(alertServiceSpy.success.calls.count()).toBe(0);
    expect(routerSpy.navigate.calls.count()).toBe(0);
  });

  it('onSubmit with create game should be give succes message when valid info is given', () => {
    const emptyGame: Game = {
      id: '',
      title: '',
      publisher: {
        name: 'test',
        founder: 'test',
        headquarterAdres: 'Kevin 23',
      },
      description: '',
      releaseYear: 0,
      characters: [],
      creatorId: '',
    };

    component.game = emptyGame;
    component.id = 'test';

    gameServiceSpy.create.and.returnValue(of(component.game));

    component.onSubmit('TestId');
    fixture.detectChanges();

    expect(component.game.creatorId).toEqual('TestId');
    expect(alertServiceSpy.success.calls.count()).toBe(1);
    expect(routerSpy.navigate.calls.count()).toBe(1);
  });

  it('onSubmit with create game should be give error message when NON valid info is given', () => {
    const emptyGame: Game = {
      id: '',
      title: '',
      publisher: {
        name: 'test',
        founder: 'test',
        headquarterAdres: 'Kevin 23',
      },
      description: '',
      releaseYear: 0,
      characters: [],
      creatorId: '',
    };

    component.game = emptyGame;
    component.id = 'test';

    gameServiceSpy.create.and.returnValue(throwError(expectedErrorResponse));

    component.onSubmit('NonExistentId');
    fixture.detectChanges();

    expect(component.game.creatorId).toEqual('NonExistentId');
    expect(alertServiceSpy.error.calls.count()).toBe(1);
    expect(alertServiceSpy.success.calls.count()).toBe(0);
    expect(routerSpy.navigate.calls.count()).toBe(0);
  });
});
