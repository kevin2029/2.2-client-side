import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Observable, of, Subscription } from 'rxjs';
import { catchError, switchMap, tap } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/auth/authentication.service';
import { User } from 'src/app/auth/user.model';
import { AlertService } from 'src/app/shared/alert';
import { Game } from '../game.model';
import { GameService } from '../game.service';

@Component({
  selector: 'app-Game-update',
  templateUrl: './game-update.component.html',
  styleUrls: ['./game-update.component.css'],
})
export class GameUpdateComponent implements OnInit, OnDestroy {
  user$: Observable<User>;
  game: Game;
  subscription: Subscription;
  id: string;

  options: {
    autoClose: true;
    keepAfterRouteChange: true;
  };

  constructor(
    private GameService: GameService,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    protected alertService: AlertService
  ) {}

  ngOnInit(): void {
    this.user$ = this.authenticationService.getUserFromLocalStorage();

    this.subscription = this.route.paramMap
      .pipe(
        tap(console.log),
        switchMap((params: ParamMap) => {
          if (!params.get('id')) {
            return of({
              title: '',
              publisher: {
                name: '',
                founder: '',
                headquarterAdres: '',
              },
              description: '',
              releaseYear: 0,
            });
          } else {
            return this.GameService.getById(params.get('id'));
          }
        }),
        tap(console.log)
      )
      .subscribe((Games) => {
        this.game = Games;
        console.log(Games.id);
        this.id = Games._id !== '' ? Games._id : 'New Game';
        this.game.id = this.id;
      });
  }

  onSubmit(creatorId: string): void {
    console.log('onSubmit', this.game.id, this.id);

    if (this.game.id) {
      console.log('update Game');
      this.GameService.update(this.game).subscribe(
        (_) => {
          this.alertService.success('Game is updated', this.options);
          this.router.navigate(['..'], { relativeTo: this.route });
        },
        (error) => {
          this.alertService.error('Something went wrong', this.options);
        }
      );
    } else {
      console.log('create Game');
      this.game.creatorId = creatorId;
      this.GameService.create(this.game).subscribe(
        (data) => {
          this.alertService.success('Game is created', this.options);
          this.router.navigate(['..', data.id], { relativeTo: this.route });
        },
        (error) => {
          this.alertService.error('Something went wrong', this.options);
        }
      );
    }
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
