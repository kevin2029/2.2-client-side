import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Game } from './game.model';
import { catchError, tap } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/auth/authentication.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class GameService {
  private gameURL = `${environment.api_url}/api/game`;

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: this.authService.getToken(),
    }),
  };

  constructor(
    private http: HttpClient,
    private authService: AuthenticationService
  ) {}

  getAll(): Observable<Game[]> {
    return this.http.get<Game[]>(this.gameURL).pipe(
      tap((_) => console.log(`get all Games`)),
      catchError(this.handleError<Game[]>('get all Games', []))
    );
  }

  getById(id: string): Observable<Game> {
    const url = `${this.gameURL}/detail/${id}`;
    return this.http.get<Game>(url).pipe(
      tap((_) => console.log(`getGameById: ${id}`)),
      catchError(this.handleError<Game>(`getGameById: ${id}`))
    );
  }

  create(Game: Game): Observable<Game> {
    return this.http
      .post<Game>(`${this.gameURL}/create`, Game, this.httpOptions)
      .pipe(
        tap((createdGame: Game) =>
          console.log(`Create Game: ${createdGame.id}`)
        ),
        catchError(this.handleError<Game>('createGame'))
      );
  }

  addToList(gameId: string, listId: string): Observable<any> {
    const url = `api/personalList/${listId}/add/game/${gameId}`;
    return this.http.put(url, this.httpOptions).pipe(
      tap((_) => console.log(`Game added to list id=${listId}`)),
      catchError(this.handleError<any>('Add Game to List'))
    );
  }

  addToGame(gameId: string, characterId: string): Observable<any> {
    const url = `${this.gameURL}/${gameId}/add/character/${characterId}`;
    return this.http.put(url, '', this.httpOptions).pipe(
      tap((_) => console.log(`Character added to game id=${characterId}`)),
      catchError(this.handleError<any>('Add Char to Game'))
    );
  }

  update(Game: Game): Observable<any> {
    const url = `${this.gameURL}/update/${Game.id}`;
    return this.http.put(url, Game, this.httpOptions).pipe(
      tap((_) => console.log(`Game Updated id=${Game.id}`)),
      catchError(this.handleError<any>('updateGame'))
    );
  }

  delete(id: string, characterId: string, type: string): Observable<Game> {
    let url = `${this.gameURL}/${id}`;
    if (type == 'character') {
      url = `${this.gameURL}/${id}/delete/character/${characterId}`;
    }
    return this.http.delete<Game>(url, this.httpOptions).pipe(
      tap((_) => console.log(`deleted : ${id}`)),
      catchError(this.handleError<Game>('deleteGame'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(operation, error);
      return of(result);
    };
  }
}
