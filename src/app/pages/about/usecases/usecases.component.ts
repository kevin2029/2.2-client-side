import { Component, OnInit } from '@angular/core';
import { UseCase } from '../usecase.model';

@Component({
  selector: 'app-about-usecases',
  templateUrl: './usecases.component.html',
  styleUrls: ['./usecases.component.css'],
})
export class UsecasesComponent implements OnInit {
  readonly user = 'Reguliere gebruiker';
  readonly admin = 'beheerder';
  readonly ADMIN_USER = 'Administrator';

  useCases: UseCase[] = [
    {
      id: 'UC-01',
      name: 'Inloggen',
      description: 'Een bestaande gebruiker kan inloggen',
      scenario: [
        'Gebruiker vult email en password in en klikt op Login knop.',
        'De applicatie valideert de ingevoerde gegevens.',
        'Indien gegevens correct zijn dan redirect de applicatie naar het startscherm.',
      ],
      actor: this.user,
      precondition: 'De actor heeft een bestaand account op de site.',
      postcondition: 'De actor is ingelogd.',
    },
    {
      id: 'UC-02',
      name: 'Registreren',
      description: 'Een nieuwe gebruiker kan een account aanmaken',
      scenario: [
        'nieuwe gebruiker vult zijn gegevens in en klikt op de register knop.',
        'De applicatie valideert de ingevoerde gegevens.',
        'Indien gegevens correct zijn dan redirect de applicatie naar het login.',
      ],
      actor: this.user,
      precondition: 'geen',
      postcondition: 'De actor is heeft een account aangemaakt.',
    },
    {
      id: 'UC-03',
      name: 'Overzicht games bekijken',
      description:
        'De games die op de website zijn weergegeven kunnen bekeken worden.',
      scenario: ['Gebruiker krijgt een overzicht van games.'],
      actor: this.user,
      precondition: 'geen',
      postcondition: 'geen',
    },
    {
      id: 'UC-04',
      name: 'Overzicht characters bekijken',
      description:
        'De characters die op de website zijn weergegeven kunnen bekeken worden',
      scenario: [
        'De characters die op de website zijn weergegeven kunnen bekeken worden.',
      ],
      actor: this.user,
      precondition: 'geen',
      postcondition: 'geen',
    },
    {
      id: 'UC-05',
      name: 'Overzicht persoonlijke lijst bekijken',
      description: 'De lijsten die een gebruiker heeft kunnen bekeken worden.',
      scenario: [
        'Er is een overzicht van lijsten beschikbaar. vanaf daar is het mogelijk om door te klikken naar een detail pagina waar je alle games van de lijst kunt zien.',
      ],
      actor: this.user,
      precondition: 'De actor heeft een bestaand account op de site.',
      postcondition: 'geen',
    },
    {
      id: 'UC-06',
      name: 'Persoonlijke lijst Aanpassen/verwijderen',
      description:
        'De lijsten die een gebruiker heeft kunnen aangepast/verwijderd worden.',
      scenario: [
        'Er is een overzicht van lijsten beschikbaar. vanaf daar is het mogelijk om door te klikken naar een detail pagina waar je alle games van de lijst kunt zien. daar is er de mogelijkheid om games toe te voegen of games te verwijderen. ook is er een mogelijkheid om de hele lijst te verwijderen.',
      ],
      actor: this.user,
      precondition: 'De actor heeft een bestaand account op de site.',
      postcondition: 'een persoonlijke lijst wordt aangepast/verwijderd.',
    },
    {
      id: 'UC-07',
      name: 'Game aanmaken/updaten/verwijderen',
      description: 'CUD functionaliteit van een Game is mogelijk.',
      scenario: [
        'op de overzicht van games kan een admin een game aanmaken/aanpassen of verwijderen.',
      ],
      actor: this.admin,
      precondition: 'De actor heeft een bestaand account op de site.',
      postcondition: 'een game wordt aangemaakt/aangepast/verwijderd.',
    },
    {
      id: 'UC-08',
      name: 'Character aanmaken/updaten/verwijderen',
      description: 'CUD functionaliteit van een Character is mogelijk.',
      scenario: [
        'op de overzicht van Characters kan een admin een Character aanmaken/aanpassen of verwijderen.',
      ],
      actor: this.admin,
      precondition: 'De actor heeft een bestaand account op de site.',
      postcondition: 'een Character wordt aangemaakt/aangepast/verwijderd.',
    },
  ];

  constructor() {}

  ngOnInit(): void {}
}
