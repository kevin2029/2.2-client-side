import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute, convertToParamMap, Router } from '@angular/router';
import { BehaviorSubject, of } from 'rxjs';
import { AuthenticationService } from 'src/app/auth/authentication.service';
import { User } from 'src/app/auth/user.model';
import { Character } from '../character.model';
import { CharacterService } from '../character.service';
import { CharacterDetailComponent } from './character-detail.component';

const expectedCharacterData: Character = {
  id: 'TestId',
  name: 'Test',
  description: 'Test',
  firstAppearance: 'Test',
  gender: 'Test',
  creatorId: 'Test',
};

const expectedUserData: User = {
  id: 'TestId',
  username: 'Firstname',
  password: 'Lastname',
  email: 'user@host.com',
  birthdate: new Date(),
  role: 'test',
};

describe('CharacterDetailComponent', () => {
  let component: CharacterDetailComponent;
  let fixture: ComponentFixture<CharacterDetailComponent>;

  let characterServiceSpy;
  let authenticationServiceSpy;
  let routerSpy;

  beforeEach(() => {
    characterServiceSpy = jasmine.createSpyObj('characterService', [
      'getById',
      'delete',
    ]);
    authenticationServiceSpy = jasmine.createSpyObj('AuthenticationService', [
      'userMayEdit',
      'checkAdmin',
    ]);
    routerSpy = jasmine.createSpyObj('Router', ['navigate']);

    const mockUser$ = new BehaviorSubject<User>(expectedUserData);
    authenticationServiceSpy.currentUser$ = mockUser$;

    TestBed.configureTestingModule({
      declarations: [CharacterDetailComponent],
      imports: [FormsModule],
      providers: [
        { provide: CharacterService, useValue: characterServiceSpy },
        { provide: AuthenticationService, useValue: authenticationServiceSpy },
        { provide: Router, useValue: routerSpy },
        {
          provide: ActivatedRoute,
          useValue: {
            paramMap: of(
              convertToParamMap({
                id: 1,
              })
            ),
          },
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(CharacterDetailComponent);
    component = fixture.componentInstance;
  });

  afterEach(() => {
    fixture.destroy();
  });

  it('should create', () => {
    characterServiceSpy.getById.and.returnValue(of(expectedCharacterData));

    fixture.detectChanges();
    expect(component).toBeTruthy();
  });
});
