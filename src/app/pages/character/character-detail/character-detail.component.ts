import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/auth/authentication.service';
import { Character } from '../character.model';
import { CharacterService } from '../character.service';

@Component({
  selector: 'app-character-detail',
  templateUrl: './character-detail.component.html',
  styleUrls: ['./character-detail.component.css'],
})
export class CharacterDetailComponent implements OnInit {
  character$: Observable<Character>;

  constructor(
    private characterService: CharacterService,
    private authenticationService: AuthenticationService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.character$ = this.route.paramMap.pipe(
      tap((params: ParamMap) => console.log('game.id = ', params.get('id'))),
      switchMap((params: ParamMap) =>
        this.characterService.getById(params.get('id'))
      ),
      tap(console.log)
    );
  }

  delete(id: string): void {
    if (confirm('Weet je zeker dat je deze character wilt verwijderen?')) {
      this.characterService.delete(id).subscribe();
      this.router.navigate(['..'], { relativeTo: this.route });
    }
  }
}
