import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/auth/authentication.service';
import { environment } from 'src/environments/environment';
import { Character } from './character.model';

@Injectable({
  providedIn: 'root',
})
export class CharacterService {
  private characterURL = `${environment.api_url}/api/character`;

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: this.authService.getToken(),
    }),
  };

  constructor(
    private authService: AuthenticationService,
    private http: HttpClient
  ) {}

  getAll(): Observable<Character[]> {
    return this.http.get<Character[]>(this.characterURL).pipe(
      tap((_) => console.log(`get all Characters`)),
      catchError(this.handleError<Character[]>('get all Characters', []))
    );
  }

  getById(id: string): Observable<Character> {
    const url = `${this.characterURL}/detail/${id}`;
    return this.http.get<Character>(url).pipe(
      tap((_) => console.log(`getRestaurantById: ${id}`)),
      catchError(this.handleError<Character>(`getRestaurantById: ${id}`))
    );
  }

  create(Character: Character): Observable<Character> {
    return this.http
      .post<Character>(
        `${this.characterURL}/create`,
        Character,
        this.httpOptions
      )
      .pipe(
        tap((createdCharacter: Character) =>
          console.log(`Create Character: ${createdCharacter.id}`)
        ),
        catchError(this.handleError<Character>('createRestaurant'))
      );
  }

  update(Character: Character): Observable<any> {
    const url = `${this.characterURL}/update/${Character.id}`;
    return this.http.put(url, Character, this.httpOptions).pipe(
      tap((_) => console.log(`Character Updated id=${Character.id}`)),
      catchError(this.handleError<any>('updateCharacter'))
    );
  }

  delete(id: string): Observable<Character> {
    const url = `${this.characterURL}/${id}`;
    return this.http.delete<Character>(url, this.httpOptions).pipe(
      tap((_) => console.log(`deleted Character: ${id}`)),
      catchError(this.handleError<Character>('deleteCharacter'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(operation, error);
      return of(result);
    };
  }
}
