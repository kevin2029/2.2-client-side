import { CharacterService } from './character.service';

describe('CharacterService', () => {
  let characterService: CharacterService;

  //mock
  let httpSpy: any;
  let authServiceSpy: any;


  beforeEach(() => {
    authServiceSpy = jasmine.createSpyObj('AuthService', ['getToken']);
    httpSpy = jasmine.createSpyObj('HttpClient', ['get', 'post']);

    characterService = new CharacterService(authServiceSpy,httpSpy)
  });

  it('should be created', () => {
    expect(characterService).toBeTruthy();
  });
});
