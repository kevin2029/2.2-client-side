export class Character {
  id: string;
  name: string;
  description: string;
  firstAppearance: string;
  gender: string;
  creatorId: string;
}
