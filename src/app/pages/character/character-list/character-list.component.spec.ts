import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { AuthenticationService } from 'src/app/auth/authentication.service';
import { User } from 'src/app/auth/user.model';
import { Character } from '../character.model';
import { CharacterService } from '../character.service';
import { CharacterListComponent } from './character-list.component';

const expectedUserData: User = {
  id: 'TestId',
  username: 'Firstname',
  password: 'Lastname',
  email: 'user@host.com',
  birthdate: new Date(),
  role: 'test',
};

const characters: Character[] = [
  {
    id: 'TestId',
    name: 'Test',
    description: 'Test',
    firstAppearance: 'Test',
    gender: 'Test',
    creatorId: 'Test',
  },
  {
    id: 'TestId',
    name: 'Test',
    description: 'Test',
    firstAppearance: 'Test',
    gender: 'Test',
    creatorId: 'Test',
  },
];
describe('CharacterListComponent', () => {
  let component: CharacterListComponent;
  let fixture: ComponentFixture<CharacterListComponent>;

  let characterServiceSpy;
  let authenticationServiceSpy;

  beforeEach(() => {
    characterServiceSpy = jasmine.createSpyObj('characterService', ['getAll']);
    authenticationServiceSpy = jasmine.createSpyObj('AuthenticationService', [
      'userMayEdit',
    ]);

    const mockUser$ = new BehaviorSubject<User>(expectedUserData);
    authenticationServiceSpy.currentUser$ = mockUser$;

    TestBed.configureTestingModule({
      declarations: [CharacterListComponent],
      providers: [
        { provide: CharacterService, useValue: characterServiceSpy },
        { provide: AuthenticationService, useValue: authenticationServiceSpy },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(CharacterListComponent);
    component = fixture.componentInstance;
  });

  afterEach(() => {
    fixture.destroy();
  });

  it('should create', (done) => {
    characterServiceSpy.getAll.and.returnValue(of(characters));
    expect(component).toBeTruthy();

    component.characters$ = of(characters);

    setTimeout(() => {
      component.characters$.subscribe((character) => {
        expect(character).toEqual(characters);
        done();
      });
    }, 200);
  });
});
