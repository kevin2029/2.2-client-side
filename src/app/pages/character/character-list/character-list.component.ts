import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthenticationService } from 'src/app/auth/authentication.service';
import { User } from 'src/app/auth/user.model';
import { Character } from '../character.model';
import { CharacterService } from '../character.service';

@Component({
  selector: 'app-character-list',
  templateUrl: './character-list.component.html',
  styleUrls: ['./character-list.component.css']
})
export class CharacterListComponent implements OnInit {

  user$: Observable<User>;
  characters$: Observable<Character[]>;

  constructor(
    private characterService: CharacterService,
    private authenticationService: AuthenticationService
  ) { }

  ngOnInit(): void {
    console.log("Character list loaded")
    this.user$ = this.authenticationService.getUserFromLocalStorage();
    this.characters$ = this.characterService.getAll();
  }
}
