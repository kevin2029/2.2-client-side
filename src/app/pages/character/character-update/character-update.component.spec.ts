import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute, convertToParamMap, Router } from '@angular/router';
import { BehaviorSubject, of, Subscription } from 'rxjs';
import { AuthenticationService } from 'src/app/auth/authentication.service';
import { User } from 'src/app/auth/user.model';
import { AlertService } from 'src/app/shared/alert';
import { Character } from '../character.model';
import { CharacterService } from '../character.service';

import { CharacterUpdateComponent } from './character-update.component';

const expectedUserData: User = {
  id: 'TestId',
  username: 'Firstname',
  password: 'Lastname',
  email: 'user@host.com',
  birthdate: new Date(),
  role: 'test',
};
const expectedCharacterData = {
  _id: 'TestId',
  name: 'Test',
  description: 'Test',
  firstAppearance: 'Test',
  gender: 'Test',
  creatorId: 'Test',
};

describe('CharacterUpdateComponent', () => {
  let component: CharacterUpdateComponent;
  let fixture: ComponentFixture<CharacterUpdateComponent>;

  let characterServiceSpy;
  let authenticationServiceSpy;
  let routerSpy;
  let alertServiceSpy;

  beforeEach(() => {
    characterServiceSpy = jasmine.createSpyObj('CharacterService', [
      'getById',
      'update',
      'create',
    ]);
    authenticationServiceSpy = jasmine.createSpyObj('AuthenticationService', [
      'userMayEdit',
      'getUserFromLocalStorage',
    ]);
    routerSpy = jasmine.createSpyObj('Router', ['navigate']);
    alertServiceSpy = jasmine.createSpyObj('AlertService', ['succes']);

    const mockUser$ = new BehaviorSubject<User>(expectedUserData);
    authenticationServiceSpy.currentUser$ = mockUser$;

    TestBed.configureTestingModule({
      declarations: [CharacterUpdateComponent],
      imports: [FormsModule],
      providers: [
        { provide: CharacterService, useValue: characterServiceSpy },
        {
          provide: AuthenticationService,
          useValue: authenticationServiceSpy,
        },
        { provide: AlertService, useValue: alertServiceSpy },
        { provide: Router, useValue: routerSpy },
        {
          provide: ActivatedRoute,
          useValue: {
            paramMap: of(
              convertToParamMap({
                id: 'TestId',
              })
            ),
          },
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(CharacterUpdateComponent);
    component = fixture.componentInstance;
  });

  afterEach(() => {
    fixture.destroy();
  });

  it('should create', (done) => {
    characterServiceSpy.getById.and.returnValue(of(expectedCharacterData));
    component.subscription = new Subscription();

    fixture.detectChanges();
    expect(component).toBeTruthy();

    setTimeout(() => {
      expect(component.character.id).toEqual('TestId');
      done();
    }, 200);
  });
});
