import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Observable, of, Subscription } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/auth/authentication.service';
import { User } from 'src/app/auth/user.model';
import { AlertService } from 'src/app/shared/alert';
import { Character } from '../character.model';
import { CharacterService } from '../character.service';

@Component({
  selector: 'app-character-update',
  templateUrl: './character-update.component.html',
  styleUrls: ['./character-update.component.css'],
})
export class CharacterUpdateComponent implements OnInit {
  user$: Observable<User>;
  character: Character;
  subscription: Subscription;
  id: string;

  options: {
    autoClose: true;
    keepAfterRouteChange: true;
  };

  constructor(
    private authenticationService: AuthenticationService,
    private alertService: AlertService,
    private characterService: CharacterService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.user$ = this.authenticationService.getUserFromLocalStorage();

    this.subscription = this.route.paramMap
      .pipe(
        tap(console.log),
        switchMap((params: ParamMap) => {
          if (!params.get('id')) {
            return of({
              name: '',
              firstAppearance: '',
              description: '',
              gender: 0,
            });
          } else {
            return this.characterService.getById(params.get('id'));
          }
        }),
        tap(console.log)
      )
      .subscribe((characters) => {
        this.character = characters;
        this.id = characters._id !== '' ? characters._id : 'New Character';
        this.character.id = this.id;
      });
  }

  onSubmit(creatorId: string): void {
    console.log('onSubmit', this.character.gender);

    if (this.character.id) {
      console.log('update Character');
      this.alertService.success('Character is updated', this.options);
      this.characterService
        .update(this.character)
        .subscribe((_) =>
          this.router.navigate(['..'], { relativeTo: this.route })
        );
    } else {
      console.log('create Character');
      this.alertService.success('Character is created', this.options);
      this.character.creatorId = creatorId;
      this.characterService
        .create(this.character)
        .subscribe((data) =>
          this.router.navigate(['..', data.id], { relativeTo: this.route })
        );
    }
  }
}
