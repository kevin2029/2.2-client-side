import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import * as fromComponents from './index';
import { FormsModule } from '@angular/forms';
import { AuthGuardGuard } from 'src/app/auth/auth-guard.guard';
import { AuthRoleGuard } from 'src/app/auth/auth-role.guard';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: fromComponents.CharacterListComponent,
  },
  {
    path: 'create',
    pathMatch: 'full',
    component: fromComponents.CharacterUpdateComponent,
    canActivate: [AuthRoleGuard, AuthGuardGuard],
  },
  {
    path: ':id',
    pathMatch: 'full',
    component: fromComponents.CharacterDetailComponent,
  },
  {
    path: ':id/update',
    pathMatch: 'full',
    component: fromComponents.CharacterUpdateComponent,
    canActivate: [AuthRoleGuard, AuthGuardGuard],
  },
];
@NgModule({
  declarations: [...fromComponents.components],
  imports: [CommonModule, RouterModule.forChild(routes), FormsModule],
})
export class CharacterModule {}
