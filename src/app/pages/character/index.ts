import { CharacterDetailComponent } from "./character-detail/character-detail.component";
import { CharacterListComponent } from "./character-list/character-list.component";
import { CharacterUpdateComponent } from "./character-update/character-update.component";



export const components: any[] = [CharacterDetailComponent, CharacterListComponent, CharacterUpdateComponent];

export * from './character-detail/character-detail.component';
export * from './character-list/character-list.component';
export * from './character-update/character-update.component'