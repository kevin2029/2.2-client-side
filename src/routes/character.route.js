const express = require("express");
const router = express.Router();
const character = require("../models/character")();

const CrudController = require("../controllers/crud");
const characterCrudController = new CrudController(character);

const authenticationController = require("../controllers/authentication.controller");

router.get("/", characterCrudController.getAll);
router.get("/detail/:id", characterCrudController.getOne);

router.post("/create", characterCrudController.create);
router.put(
    "/update/:id",
    authenticationController.validateToken,
    characterCrudController.update
);
router.delete(
    "/:id",
    authenticationController.validateToken,
    characterCrudController.deleteCharacter
);

module.exports = router;