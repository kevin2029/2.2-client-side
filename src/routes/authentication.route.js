const express = require("express");
const router = express.Router();

const authcontroller = require("../controllers/authentication.controller");

// register
router.post("/register", authcontroller.register);

// register admin
router.post("/register/admin", authcontroller.register);

// login
router.post("/login", authcontroller.login);

module.exports = router;