const express = require("express");
const router = express.Router();
const game = require("../models/game")();

const CrudController = require("../controllers/crud");
const gameCrudController = new CrudController(game);
const gameController = require("../controllers/game.controller");
const SpecialGameController = new gameController(game);

const authenticationController = require("../controllers/authentication.controller");

router.get("/", gameCrudController.getAll);
router.get("/:id", gameCrudController.getOne);
router.get("/detail/:id", SpecialGameController.getAllCharacter);

router.post(
    "/create",
    authenticationController.validateToken,
    gameCrudController.create
);
router.put(
    "/update/:id",
    authenticationController.validateToken,
    gameCrudController.update
);

router.delete(
    "/:id",
    authenticationController.validateToken,
    SpecialGameController.deleteAll
);

//character
router.put(
    "/:id/add/character/:characterId",
    SpecialGameController.addCharacter
);

router.delete(
    "/:id/delete/character/:characterId",
    authenticationController.validateToken,
    SpecialGameController.delete
);

module.exports = router;