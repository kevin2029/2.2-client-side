const express = require("express");
const router = express.Router();
const personalList = require("../models/personalList")();

const authenticationController = require("../controllers/authentication.controller");
const controller = require("../controllers/personalList.controller");
const personalListController = new controller(personalList);

//list
//get all lists for user.
router.get("/:id", personalListController.getByUserId);

//getOne
router.get("/one/:id", personalListController.getList);

router.post(
    "/create",
    authenticationController.validateToken,
    personalListController.create
);

router.put("/:id/update/", personalListController.updateListName);

router.delete(
    "/:id",
    authenticationController.validateToken,
    personalListController.delete
);

//game
//add
router.put("/:id/add/game/:gameId", personalListController.addToList);

//remove
router.delete(
    "/:id/delete/game/:gameId",
    authenticationController.validateToken,
    personalListController.deleteFromList
);

module.exports = router;