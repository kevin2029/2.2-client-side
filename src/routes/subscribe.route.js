const express = require("express");
const router = express.Router();

const subscriptionController = require("../controllers/subscription.controller");
const authenticationController = require("../controllers/authentication.controller");

//gets all the subscription of a user
router.get(
    "/:userId",
    authenticationController.validateToken,
    subscriptionController.getSubscripton
);

router.get(
    "/:userId/:type",
    authenticationController.validateToken,
    subscriptionController.getSubscripton
);

//subsribes to a user
router.post(
    "/subscribe",
    authenticationController.validateToken,
    subscriptionController.checkSubscription,
    subscriptionController.subscibe
);

router.delete(
    "/unSubscribe",
    authenticationController.validateToken,
    subscriptionController.unSubscibeCheck,
    subscriptionController.unSubscibe
);
module.exports = router;