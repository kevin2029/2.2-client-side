const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const getModel = require("../model_cache");

// define the reservering schema
const UserSchema = new Schema({
    username: {
        type: String,
        required: [true, "Een gebruiker moet een username hebben."],
        unique: [true, "Gebruikersnaam al in gebruik."],
    },
    password: {
        type: String,
        required: [true, "Een gebruiker moet een password hebben."],
    },
    email: {
        type: String,
        trim: true,
        lowercase: true,
        unique: true,
        required: [true, "Geef een e-mail adres op."],
        match: [
            /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
            "Geef een valide e-mail adres",
        ],
    },
    birthdate: {
        type: Date,
        required: [true, "Een gebruiker moet een geboortedatum hebben."],
    },
    role: {
        type: String,
        required: [true, "Een gebruiker moet een role hebben."],
    },
});

module.exports = getModel("user", UserSchema);