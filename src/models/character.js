const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const getmodel = require("./model_cache");

const CharacterSchema = new Schema({
    name: {
        type: String,
        required: [true, "Geef de naam op van de character"],
    },
    firstAppearance: {
        type: String,
        required: [
            true,
            "Geef op bij welke game de character voor het eerst voor kwam.",
        ],
    },
    description: {
        type: String,
    },
    gender: {
        type: String,
        enum: ["Male", "Female", "Other"],
        required: [true, "Geef de sex op"],
    },
    creatorId: {
        type: String,
    },
});

module.exports = getmodel("character", CharacterSchema);