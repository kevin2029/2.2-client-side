const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const getmodel = require("./model_cache");
const PublisherSchema = require("./schemas/publisher.schema");

const GameSchema = new Schema({
    title: {
        type: String,
        required: [true, "Geef de titel van de game."],
    },
    publisher: {
        type: PublisherSchema,
        required: [true, "Geef informatie over de publisher op."],
    },
    description: {
        type: String,
    },
    releaseYear: {
        type: Number,
        required: [true, "Geef het jaar op."],
    },
    characters: [{
        type: Schema.Types.ObjectId,
        ref: "character",
    }, ],
    creatorId: {
        type: String,
    },
});

module.exports = getmodel("game", GameSchema);