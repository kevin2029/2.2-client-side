const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const PublisherSchema = new Schema({
    name: {
        type: String,
        required: [true, "Geef een naam op."],
    },
    founder: {
        type: String,
    },
    headquarterAdres: {
        type: String,
        required: [true, "geef een adres op."],
    },
});

module.exports = PublisherSchema;