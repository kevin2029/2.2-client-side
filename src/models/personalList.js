const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const getmodel = require("./model_cache");

const personalListSchema = new Schema({
    userId: {
        type: Schema.Types.ObjectId,
        ref: "user",
        required: [true, "Geef UserId mee."],
    },
    name: {
        type: String,
        required: [true, "Geef een naam op"],
    },
    games: [{
        type: Schema.Types.ObjectId,
        ref: "game",
    }, ],
    dateCreated: {
        type: String,
        required: [true, "Datum nodig."],
    },
});

module.exports = getmodel("personalList", personalListSchema);