const neo4j = require("neo4j-driver");

function connect(dbName) {
    this.dbName = dbName;
    this.driver = neo4j.driver(
        process.env.NEO4J_URL,
        neo4j.auth.basic(process.env.NEO4J_USER, process.env.NEO4J_PASSWORD)
    );
}

function session() {
    return this.driver.session({
        database: this.dbName,
        defaultAccessMode: neo4j.session.WRITE,
    });
}

module.exports = {
    connect,
    session,
    dropAll: "MATCH (n) DETACH DELETE n",
    getAll: `MATCH (n) RETURN n;`,
    findUser: "MATCH(user:User{username:$username}) RETURN user",
    createUser: "CREATE (user:User {id: $id, username: $username, password: $password, role: $role})",
    checkSubscription: "RETURN EXISTS( (:User {id: $userId})-[:Subscription]->(:User {id: $otherUserId}))",
    getSubscription: "MATCH(:User {id: $userId})-[r:Subscription]->(users:User) RETURN users",
    getSubscriptionToMe: "MATCH (:User {id: $userId})<-[r:Subscription]-(users:User) RETURN users",
    subscribe: "MATCH (user:User {id: $userId}), (otherUser:User {id: $otherUserId}) CREATE (user)-[r:Subscription]->(otherUser)",
    unSubscribe: "MATCH (user:User {id: $userId})-[r:Subscription]->(otherUser:User {id: $otherUserId}) DELETE r",
};