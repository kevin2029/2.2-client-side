require("dotenv").config();
require("express-async-errors");
const express = require("express");
const path = require("path");

const bodyParser = require("body-parser");
const compression = require("compression");

//routes
const gameRoutes = require("./routes/game.route");
const authenticationRoutes = require("./routes/authentication.route");
const characterRoutes = require("./routes/character.route");
const personalListRoutes = require("./routes/personalList.route");
const subscriptionRoutes = require("./routes/subscribe.route");
const app = express();

app.use(bodyParser.json());
app.use(compression());

const appname = "Movie";

// Point static path to dist
app.use(express.static(path.join(__dirname, "..", "dist", appname)));

// Logging requests that come in for debugging purposes
app.all("*", (req, res, next) => {
    const method = req.method;
    const url = req.url;
    console.log("The used method is: ", method, " on url: ", url);
    next();
});

//API Routes
app.use("/api/game", gameRoutes);
app.use("/api/authentication", authenticationRoutes);
app.use("/api/character", characterRoutes);
app.use("/api/personalList", personalListRoutes);
app.use("/api/subscription", subscriptionRoutes);

const errors = require("../src/errors");

// catch non-existing endpoints
app.use("*", function(_, res) {
    res.status(404).end();
});

// catch error responses
app.use("*", function(err, req, res, next) {
    console.error(`${err.name}: ${err.message}`);
    next(err);
});

app.use("*", errors.handlers);

app.use("*", function(err, req, res, next) {
    res.status(500).json({
        message: "An unexpected error occurred.",
    });
});

module.exports = app;