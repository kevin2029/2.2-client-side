require("dotenv").config();
const connect = require("./connect");
const http = require("http");

const app = require("../src/app");
const appname = "Movie";
const port = process.env.PORT || "3000";

app.set("port", port);
// Create HTTP server.
const server = http.createServer(app);
// Listen on provided port, on all network interfaces.
server.listen(port, () => {
    console.log(
        `Angular app \'${appname}\' running in ${process.env.NODE_ENV} mode on port ${port}`
    );
});

connect.mongo(process.env.MONGO_PROD_DB);
connect.neo(process.env.NEO4J_PROD_DB);