const gameObject = require("../models/game")();
class personalListController {
    constructor(model) {
        this.model = model;
    }
    getByUserId = async(req, res, next) => {
        console.log("In getByUserId");

        //get the lists.
        const lists = await this.model.find({ userId: req.params.id });

        //for everylist
        for (let i = 0; i < lists.length; i++) {
            //inject gamedata in the list
            for (let x = 0; x < lists[i].games.length; x++) {
                const gameId = lists[i].games[x]._id;
                const game = await gameObject.findById(gameId);
                const inject = {
                    gameId: gameId,
                    title: game.title,
                    publisher: game.publisher,
                    description: game.description,
                    releaseYear: game.releaseYear,
                    characters: game.characters,
                    userId: game.userId,
                };

                lists[i].games[x] = inject;
            }
        }
        res.status(200).send(lists);
    };

    create = async(req, res, next) => {
        console.log("in create personalList");

        let today = new Date();
        let dd = String(today.getDate()).padStart(2, "0");
        let mm = String(today.getMonth() + 1).padStart(2, "0");
        let yyyy = today.getFullYear();

        today = mm + "/" + dd + "/" + yyyy;

        //add datecreated to object
        const listToAdd = {
            userId: req.body.userId,
            name: req.body.name,
            games: [],
            dateCreated: today,
        };

        try {
            const entity = new this.model(listToAdd);
            await entity.save();
            res.status(201).json(entity);
        } catch (error) {
            res.status(400).json({
                error: "You already have a list with this name!",
            });
        }
    };

    getList = async(req, res, next) => {
        console.log("in get list.");

        //get the list.
        const list = await this.model.findById(req.params.id);

        for (let i = 0; i < list.games.length; i++) {
            const gameId = list.games[i]._id;

            const game = await gameObject.findById(gameId);

            const inject = {
                gameId: gameId,
                title: game.title,
                publisher: game.publisher,
                description: game.description,
                releaseYear: game.releaseYear,
                characters: game.characters,
                userId: game.userId,
            };

            list.games[i] = inject;
        }
        res.status(200).send(list);
    };

    addToList = async(req, res, next) => {
        console.log("in adding game to list");

        //find list to add the game
        this.model.findOneAndUpdate({ _id: req.params.id }, {
                $addToSet: {
                    games: req.params.gameId,
                },
            }, {
                upsert: true,
                new: true,
            },
            function(err, res) {
                if (err) {
                    console.log(err);
                } else {
                    console.log(res);
                }
            }
        );
        res.status(200).json({
            message: "succes!",
        });
    };

    deleteFromList = async(req, res, next) => {
        console.log("in deleteFromList");

        this.model.findOneAndUpdate({ _id: req.params.id }, { $pull: { games: req.params.gameId } }, { upsert: true, new: true, multi: false },
            function(error, result) {
                if (error) console.log(error);
                else {
                    console.log("Alert deleted", result);
                }
            }
        );
        res.status(204).end();
    };

    delete = async(req, res, next) => {
        console.log("In Delete");
        const entity = await this.model.findById(req.params.id);
        await entity.delete();
        res.status(200).json({
            message: "succes",
        });
    };

    updateListName = async(req, res, next) => {
        console.log("in updateListName");

        const newName = req.body.name;
        this.model.findByIdAndUpdate(
            req.params.id, { name: newName },
            function(error, result) {
                if (error) {
                    console.log(error);
                } else {
                    console.log("Updated Name : ", result);
                }
            }
        );
        res.status(204).end();
    };
}
module.exports = personalListController;