const gameModel = require("../models/game")();

class CrudController {
    constructor(model) {
        this.model = model;
    }

    create = async(req, res, next) => {
        const entity = new this.model(req.body);
        await entity.save();
        res.status(201).json({ id: entity.id });
    };

    getAll = async(req, res, next) => {
        const entities = await this.model.find();
        res.status(200).send(entities);
    };

    getOne = async(req, res, next) => {
        const entity = await this.model.findById(req.params.id);
        res.status(200).send(entity);
    };

    update = async(req, res, next) => {
        await this.model.findByIdAndUpdate(req.params.id, req.body);
        res.status(204).end();
    };

    delete = async(req, res, next) => {
        // this happens in two steps to make mongoose middleware run
        const entity = await this.model.findById(req.params.id);
        await entity.delete();
        res.status(204).end();
    };

    deleteCharacter = async(req, res, next) => {
        console.log("in character delete");
        let list = [];

        const awn = await gameModel.find({
                characters: { $in: [req.params.id] },
            },
            function(error) {
                if (error) console.log(error);
            }
        );

        for (let i = 0; i < awn.length; i++) {
            list.push(awn[i]._id);
        }

        for (let i = 0; i < list.length; i++) {
            await gameModel.findOneAndUpdate({ _id: list[i] }, { $pull: { characters: req.params.id } }, { upsert: true, new: true, multi: false },
                function(error) {
                    if (error) console.log(error);
                }
            );
        }

        const entity = await this.model.findById(req.params.id);
        await entity.delete();

        res.status(200).json(awn);
    };
}

module.exports = CrudController;