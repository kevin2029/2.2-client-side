const neo = require("../neo");

function getValue(result) {
    let users = [];

    for (let user of result) {
        users.push({
            id: user._fields[0].properties.id,
            username: user._fields[0].properties.username,
        });
    }
    return users;
}

let subscriptionController = {
    async getSubscripton(req, res, next) {
        console.log("In getting subscriptions");
        const session = neo.session();

        let type = "getSubscription";

        if (req.params.type === "myself") {
            type = "getSubscriptionToMe";
        }

        const subscriptions = await session.run(neo[type], {
            userId: req.params.userId,
        });

        session.close();

        const value = await getValue(subscriptions.records);

        res.status(200).json(value);
    },

    async subscibe(req, res, next) {
        const session = neo.session();

        console.log("In subscribe");
        await session.run(neo.subscribe, {
            userId: req.body.userId,
            otherUserId: req.body.otherUserId,
        });
        session.close();

        res.status(200).json({
            message: "succes!",
        });
    },

    async unSubscibe(req, res, next) {
        const session = neo.session();

        await session.run(neo.unSubscribe, {
            userId: req.body.userId,
            otherUserId: req.body.otherUserId,
        });

        session.close();

        res.status(200).json({
            message: "succes!",
        });
    },

    async unSubscibeCheck(req, res, next) {
        const session = neo.session();

        const checkSubscription = await session.run(neo.checkSubscription, {
            userId: req.body.userId,
            otherUserId: req.body.otherUserId,
        });

        session.close();

        if (checkSubscription.records[0]._fields[0] === false) {
            res
                .status(400)
                .json({ message: "U bent niet geabboneerd op deze persoon." });
        } else {
            next();
        }
    },

    async checkSubscription(req, res, next) {
        const session = neo.session();

        const checkSubscription = await session.run(neo.checkSubscription, {
            userId: req.body.userId,
            otherUserId: req.body.otherUserId,
        });

        session.close();

        if (checkSubscription.records[0]._fields[0] === true) {
            res
                .status(400)
                .json({ message: "U bent al geabboneerd op deze persoon." });
        } else {
            next();
        }
    },
};
module.exports = subscriptionController;