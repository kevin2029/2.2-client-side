const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const neo = require("../neo");
const User = require("../models/authentication/user")();
const saltRounds = 10;

async function login(req, res) {
    const session = neo.session();

    let dbresult = await session.run(neo.findUser, {
        username: req.body.username,
    });

    // checking if there is a result from the db
    if (dbresult.records.length === 0) {
        console.log("User not found or password is invalid");
        res.status(400).json({
            error: "User not found or password is invalid",
            datetime: new Date().toISOString(),
        });
    } else {
        dbresult = dbresult.records[0]._fields[0].properties;

        // bcrypt unhashing the password
        bcrypt.compare(
            req.body.password,
            dbresult.password,
            function(err, result) {
                if (err) {
                    console.log(err);
                    res.status(400).json({
                        error: "User not found or password is invalid",
                        datetime: new Date().toISOString(),
                    });
                }

                if (result == false) {
                    res.status(400).json({
                        error: "User not found or password is invalid",
                        datetime: new Date().toISOString(),
                    });
                }
                if (result) {
                    console.log("passwords DID match, sending valid token");
                    let payload = {
                        ID: dbresult.id,
                        IsAdmin: 0,
                    };
                    if (dbresult.role == "admin") {
                        payload = {
                            ID: dbresult.id,
                            IsAdmin: 1,
                        };
                    }

                    // Userinfo returned to the caller.
                    const userinfo = {
                        token: jwt.sign(payload, "secret", {
                            expiresIn: "2h",
                        }),
                        id: dbresult.id,
                        username: dbresult.username,
                        role: dbresult.role,
                    };
                    res.status(200).json(userinfo);
                }
            }
        );
    }
}

async function register(req, res) {
    console.log("register");

    const session = neo.session();

    let check = await session.run(neo.findUser, {
        username: req.body.username,
    });

    if (check.records.length !== 0) {
        console.log("A user with username: ", req.body.username, " already exists");
        res.status(500).json({
            error: "Username is already taken",
            datetime: new Date().toISOString(),
        });
    } else {
        // bcrypt hashing the password
        console.log("bcrypt hasing the password");
        bcrypt.genSalt(saltRounds, function(err, salt) {
            bcrypt.hash(req.body.password, salt, function(err, hash) {
                console.log("password hashed: ", hash);

                let user = req.body;
                let role = "";
                if (req.url !== "/register/admin") {
                    role = "user";
                } else {
                    role = "admin";
                }
                user.role = role;
                console.log("creating ", role);

                const entity = new User(user);
                entity.password = hash;
                entity.save();

                session.run(neo.createUser, {
                    id: entity.id,
                    username: req.body.username,
                    password: hash,
                    role: role,
                });

                console.log("user added");
                res.status(200).json({
                    message: "user added",
                });
            });
        });
    }
}

async function validateToken(req, res, next) {
    console.log("validateToken called");

    const authHeader = req.headers.authorization;

    console.log(authHeader);
    if (!authHeader) {
        console.log("No auth header!");
        res.status(401).json({
            error: "Authorization header missing!",
            datetime: new Date().toISOString(),
        });
    } else {
        jwt.verify(authHeader, "secret", (err, payload) => {
            if (err) {
                console.log("Not authorized");
                res.status(401).json({
                    error: "Not authorized",
                    datetime: new Date().toISOString(),
                });
            }
            if (payload) {
                console.log("token is valid", payload);
                req.id = payload.id;
                next();
            }
        });
    }
}

async function validateAdmin(req, res, next) {
    console.log("validateAdmin called");
    const authHeader = req.headers.authorization;
    if (!authHeader) {
        res.status(401).json({
            error: "Authorization header missing!",
            datetime: new Date().toISOString(),
        });
    } else {
        // Strip the word 'Bearer ' from the headervalue
        const token = authHeader.substring(7, authHeader.length);

        jwt.verify(token, "secret", (err, payload) => {
            if (err) {
                console.log("Not authorized");
                res.status(401).json({
                    error: "Not authorized",
                    datetime: new Date().toISOString(),
                });
            }
            if (payload) {
                console.log("token is valid", payload);

                if (payload.IsAdmin === 1) {
                    req.id = payload.id;
                    next();
                } else {
                    res.status(401).json({
                        error: "Not authorized",
                        datetime: new Date().toISOString(),
                    });
                }
            }
        });
    }
}

module.exports = {
    login,
    register,
    validateToken,
    validateAdmin,
};