import { CommonPageObject } from '../../common.po';
import { by, element, ElementFinder } from 'protractor';

export class GameUpdatePage extends CommonPageObject {
  getTitleText(): Promise<string> {
    return element(by.id('title')).getText() as Promise<string>;
  }

  get inputTitle(): ElementFinder {
    return element(by.id('title')) as ElementFinder;
  }
  get inputPublisher(): ElementFinder {
    return element(by.id('publisherName')) as ElementFinder;
  }
  get inputDescription(): ElementFinder {
    return element(by.id('description')) as ElementFinder;
  }
  get inputReleaseYear(): ElementFinder {
    return element(by.id('releaseYear')) as ElementFinder;
  }
  get inputButtonSubmit(): ElementFinder {
    return element(by.id('buttonSubmit')) as ElementFinder;
  }
  get inputButtonBack(): ElementFinder {
    return element(by.id('buttonBack')) as ElementFinder;
  }

  get hiddenTitleError(): ElementFinder {
    return element(by.id('hiddenTitleError')) as ElementFinder;
  }
}
