import { browser, logging, protractor } from 'protractor';
import { GameUpdatePage } from './game-update.po';

describe('workspace-project GameUpdatePage', () => {
  let page: GameUpdatePage;

  beforeEach(() => {
    page = new GameUpdatePage();
    browser.waitForAngularEnabled(false);
    page.navigateTo('/games');
    page.navigateTo('/games/6067782282cff84a84a6bf33/update');

    const script =
      "window.localStorage.setItem('currentuser', JSON.stringify({token: 'token'}))";

    browser.executeScript(script);
  });

  it('should be on the gameUpdatePage', () => {
    page.navigateTo('/games/6067782282cff84a84a6bf33/update');
    browser.sleep(1000);
    expect(browser.getCurrentUrl()).toContain(
      '/games/6067782282cff84a84a6bf33/update'
    );
  });

  it('should load all the fields', () => {
    page.navigateTo('/games/6067782282cff84a84a6bf33/update');
    browser.sleep(1000);
    expect(page.inputTitle.isDisplayed()).toBeTruthy();
    expect(page.inputPublisher.isDisplayed()).toBeTruthy();
    expect(page.inputDescription.isDisplayed()).toBeTruthy();
    expect(page.inputReleaseYear.isDisplayed()).toBeTruthy();
    expect(page.inputButtonBack.isDisplayed()).toBeTruthy();
    expect(page.inputButtonSubmit.isDisplayed()).toBeTruthy();
    expect(page.inputButtonBack.isEnabled()).toBe(true);
    expect(page.inputButtonSubmit.isEnabled()).toBe(true);
  });

  it('should disable the submit button when one field is empty and show error', () => {
    page.navigateTo('/games/6067782282cff84a84a6bf33/update');
    browser.sleep(1000);

    page.inputTitle.click();
    page.inputTitle.sendKeys(protractor.Key.chord(protractor.Key.CONTROL, 'a'));
    page.inputTitle.sendKeys(protractor.Key.BACK_SPACE);

    expect(page.hiddenTitleError.isDisplayed()).toBeTruthy();
    expect(page.hiddenTitleError.getText()).toBe('Geef een titel op.');
    expect(page.inputButtonSubmit.isEnabled()).toBe(false);
  });

  it('should go back when updated game is submitted', () => {
    page.navigateTo('/games/6067782282cff84a84a6bf33/update');
    browser.sleep(1000);

    page.inputTitle.click();
    page.inputTitle.sendKeys(protractor.Key.chord(protractor.Key.CONTROL, 'a'));
    page.inputTitle.sendKeys(protractor.Key.BACK_SPACE);

    page.inputTitle.sendKeys('New title!');
    expect(page.inputButtonSubmit.isDisplayed()).toBeTruthy();
    browser.actions().mouseMove(page.inputButtonSubmit).click();
    expect(browser.getCurrentUrl()).toContain('/games');
  });

  afterEach(async () => {
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);

    expect(logs).not.toContain(
      jasmine.objectContaining({
        level: logging.Level.SEVERE,
      } as logging.Entry)
    );
    browser.executeScript("window.localStorage.removeItem('currentuser')");
  });
});
