import { browser, by, element, ElementFinder } from 'protractor';

export class GamePage {
  navigateTo(string): Promise<unknown> {
    return browser.get(browser.baseUrl + string) as Promise<unknown>;
  }
  get games(): ElementFinder {
    return element(by.id('tableGames')) as ElementFinder;
  }

  get columns(): ElementFinder {
    return element(by.id('columnTitle')) as ElementFinder;
  }
}
