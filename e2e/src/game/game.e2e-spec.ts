import { protractor } from 'protractor/built/ptor';
import { browser, by, element, logging } from 'protractor';
import { GamePage } from './game.po';

describe('workspace-project GameList', () => {
  let page: GamePage;

  beforeEach(() => {
    page = new GamePage();
    browser.waitForAngularEnabled(false);
    page.navigateTo('/games');
  });

  it('should be on the gamelist page', () => {
    page.navigateTo('/games');
    expect(browser.getCurrentUrl()).toContain('/games');
  });

  it('should show list', () => {
    expect(page.games.isDisplayed()).toBeTruthy();
  });

  it('should show detail button', () => {
    expect(element.all(by.id('detail')).count()).toEqual(3);
  });

  it('should show list items', () => {
    expect(element.all(by.tagName('tr')).get(0).isDisplayed()).toBeTruthy();
  });

  it('should show list column titles', () => {
    expect(page.columns.isDisplayed()).toBeTruthy();
  });

  afterEach(async () => {
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(
      jasmine.objectContaining({
        level: logging.Level.SEVERE,
      } as logging.Entry)
    );
  });
});
