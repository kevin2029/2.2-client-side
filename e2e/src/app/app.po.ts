import { CommonPageObject } from '../../common.po';
import { by, element, ElementFinder } from 'protractor';

export class AppPage extends CommonPageObject {
  getTitleText(): Promise<string> {
    return element(by.css('.navbar-brand')).getText() as Promise<string>;
  }
}
