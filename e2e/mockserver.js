const cors = require("cors");
const helmet = require("helmet");
const express = require("express");
const port = 3000;

let app = express();
let routes = require("express").Router();

app.use(cors());
app.use(helmet());

routes.post("/api/login", (req, res, next) => {
    res.status(200).json({
        firstName: "Firstname",
        lastName: "Lastname",
        email: "first.last@avans.nl",
        token: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1NzUwMjIzNjQsImlhdCI6MTU3NDE1ODM2NCwic3ViIjp7ImVtYWlsIjoiYWRtaW5AYXZhbnMubmwiLCJpZCI6IjVkYzlhY2Y3NmUzOTVhMTY1ODkwMjk2MiJ9fQ.qRPy-lTPIopAJPrarJYZkxK0suUJF_XZ9szeTtie4nc",
    });
});

routes.get("/api/game", (req, res, next) => {
    res.status(200).json([{
            characters: ["606771ac12fc4d0af8ca9dcf", "607f33065702a90c84d9e8c8"],
            _id: "6067782282cff84a84a6bf33",
            title: "Luigi",
            publisher: {
                name: "test",
                founder: "test",
                headquarterAdres: "Kevin 23",
            },
            description: "multiplayergame",
            releaseYear: 2000,
            __v: 0,
        },
        {
            characters: [],
            _id: "60699fd7f43758307cba34ac",
            title: "MArio",
            publisher: {
                name: "test",
                founder: "test",
                headquarterAdres: "Kevin 23",
            },
            description: "multiplayergame",
            releaseYear: 2000,
            __v: 0,
        },
        {
            characters: [],
            _id: "606c5c3743e5884ec89561cb",
            title: "Peach",
            publisher: {
                name: "test",
                founder: "test",
                headquarterAdres: "Kevin 23",
            },
            description: "Test5",
            releaseYear: 2000,
            __v: 0,
        },
    ]);
});

routes.get("/api/game/detail/6067782282cff84a84a6bf33", (req, res, next) => {
    res.status(200).json({
        characters: ["606771ac12fc4d0af8ca9dcf", "607f33065702a90c84d9e8c8"],
        _id: "6067782282cff84a84a6bf33",
        title: "Luigi",
        publisher: {
            name: "test",
            founder: "test",
            headquarterAdres: "Kevin 23",
        },
        description: "multiplayergame",
        releaseYear: 2000,
        __v: 0,
    });
});

routes.put("/api/game/update/6067782282cff84a84a6bf33", (req, res, next) => {
    res.status(204).json();
});

// Finally add your routes to the app
app.use(routes);

app.use("", function(req, res, next) {
    next({ error: "Non-existing endpoint" });
});

app.use((err, req, res, next) => {
    res.status(400).json(err);
});

app.listen(port, () => {
    console.log("Mock backend server running on port", port);
});