import { browser } from 'protractor';

export class CommonPageObject {
  navigateTo(path: string): Promise<unknown> {
    return browser.get(browser.baseUrl + path) as Promise<unknown>;
  }
}
