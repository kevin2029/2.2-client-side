require("dotenv").config();

const chai = require("chai");
const expect = chai.expect;

const jwt = require("jsonwebtoken");

const requester = require("../requester.spec");

const Game = require("../../src/models/game")();
const Character = require("../../src/models/character")();

describe("Character endpoints", function() {
    describe("integration tests", function() {
        describe("getting characters", function() {
            it("(GET / ) should return all the characters", async function() {
                const testCharacter = {
                    name: "FirstTest",
                    firstAppearance: "In this test",
                    description: "in testing",
                    gender: "Other",
                };
                const testCharacter1 = {
                    name: "SeccondTest",
                    firstAppearance: "In this test",
                    description: "in testing",
                    gender: "Other",
                };

                await new Character(testCharacter).save();
                await new Character(testCharacter1).save();

                const res = await requester.get("/api/character");
                const characters = res.body;

                expect(res).to.have.status(200);
                expect(characters).to.have.length(2);
                expect(characters[0]).to.have.property("name", "FirstTest");
                expect(characters[1]).to.have.property("name", "SeccondTest");
            });

            it("(GET /detail/:id) should return one character", async function() {
                const testCharacter = {
                    name: "FirstTest",
                    firstAppearance: "In this test",
                    description: "in testing",
                    gender: "Other",
                };

                const characterRes = await new Character(testCharacter).save();

                const res = await requester.get(
                    `/api/character/detail/${characterRes._id}`
                );
                const characters = res.body;

                expect(res).to.have.status(200);
                expect(characters).to.have.property("name", "FirstTest");
                expect(characters).to.have.property("firstAppearance", "In this test");
                expect(characters).to.have.property("description", "in testing");
            });
        });
        describe("creating characters", function() {
            it("(POST /create) should make a new character", async function() {
                const testCharacter = {
                    name: "FirstTest",
                    firstAppearance: "In this test",
                    description: "in testing",
                    gender: "Other",
                };

                const res = await requester
                    .post("/api/character/create")
                    .set("authorization", jwt.sign({ id: 1 }, "secret"))
                    .send(testCharacter);

                const character = await Character.findById(res.body.id);

                expect(res).to.have.status(201);
                expect(res).to.be.an("object");
                expect(character).to.have.property("firstAppearance", "In this test");
                expect(character).to.have.property("description", "in testing");
            });
        });

        describe("update characters", function() {
            it("(PUT /update/:id) should update one character", async function() {
                const testCharacter = {
                    name: "FirstTest",
                    firstAppearance: "In this test",
                    description: "in testing",
                    gender: "Other",
                };

                const characterRes = await new Character(testCharacter).save();

                const updatedCharacter = {
                    name: "NewName",
                    firstAppearance: "In this test",
                    description: "in UpdateTest",
                    gender: "Other",
                };

                const res = await requester
                    .put(`/api/character/update/${characterRes._id}`)
                    .set("authorization", jwt.sign({ id: 1 }, "secret"))
                    .send(updatedCharacter);

                expect(res).to.have.status(204);

                const character = await Character.findById(characterRes._id);
                expect(character).to.have.property("name", "NewName");
                expect(character).to.have.property("description", "in UpdateTest");
            });
        });

        describe("deleting characters", function() {
            it("(DELETE /:id) should delete one character", async function() {
                const testCharacter = {
                    name: "FirstTest",
                    firstAppearance: "In this test",
                    description: "in testing",
                    gender: "Other",
                };

                const characterRes = await new Character(testCharacter).save();

                const testgame1 = {
                    title: "test",
                    publisher: {
                        name: "test",
                        founder: "test",
                        headquarterAdres: "Test 21",
                    },
                    description: "test",
                    releaseYear: 2010,
                    characters: [characterRes._id],
                    creatorId: "",
                };

                const game = await new Game(testgame1).save();

                const res = await requester
                    .delete(`/api/character/${characterRes._id}`)
                    .set("authorization", jwt.sign({ id: 1 }, "secret"));

                expect(res).to.have.status(200);

                const gameRes = await Game.findById(game._id);

                expect(gameRes).to.have.property("characters").and.to.be.empty;
                expect(gameRes.characters).to.have.length(0);
            });
        });
    });
});