require("dotenv").config();

const chai = require("chai");
const expect = chai.expect;

const jwt = require("jsonwebtoken");
const requester = require("../requester.spec");

const PersonalList = require("../../src/models/personalList")();
const Game = require("../../src/models/game")();
const User = require("../../src/models/authentication/user")();

describe("personalList endpoints", function() {
    describe("integration tests", function() {
        describe("getting lists", function() {
            it("(GET /:id) should return all personalLists for one user", async function() {
                const testUser = {
                    username: "test",
                    password: "mock",
                    email: "test@test.com",
                    birthdate: new Date(),
                    role: "user",
                };

                const testUserRes = await new User(testUser).save();

                const personalList = {
                    userId: testUserRes._id,
                    name: "test",
                    games: [],
                    dateCreated: new Date(),
                };

                const personalList1 = {
                    userId: testUserRes._id,
                    name: "test1",
                    games: [],
                    dateCreated: new Date(),
                };

                await new PersonalList(personalList).save();
                await new PersonalList(personalList1).save();

                const res = await requester
                    .get(`/api/personalList/${testUserRes._id}`)
                    .set("authorization", jwt.sign({ id: 1 }, "secret"));

                const content = res.body;

                expect(res).to.have.status(200);
                expect(content[0]).to.have.property(
                    "userId",
                    testUserRes._id.toString()
                );
                expect(content[0]).to.have.property("games").and.to.be.empty;

                expect(content).to.have.length(2);
                expect(content[1]).to.have.property("name", "test1");
            });

            it("(GET /one/:id should return one list", async function() {
                const testUser = {
                    username: "test",
                    password: "mock",
                    email: "test@test.com",
                    birthdate: new Date(),
                    role: "user",
                };

                const testUserRes = await new User(testUser).save();

                const personalList = {
                    userId: testUserRes._id,
                    name: "test",
                    games: [],
                    dateCreated: new Date(),
                };

                const testPersonalListRes = await new PersonalList(personalList).save();

                const res = await requester
                    .get(`/api/personalList/one/${testPersonalListRes._id}`)
                    .set("authorization", jwt.sign({ id: 1 }, "secret"));

                const content = res.body;
                expect(res).to.have.status(200);
                expect(content).to.have.property("userId", testUserRes._id.toString());
                expect(content).to.have.property("games").and.to.be.empty;
                expect(content).to.have.property("name", "test");
            });
        });

        describe("creating a new list", function() {
            it("(POST /create) should make one personalList", async function() {
                const testUser = {
                    username: "test",
                    password: "mock",
                    email: "test@test.com",
                    birthdate: new Date(),
                    role: "user",
                };

                const testUserRes = await new User(testUser).save();

                const personalList = {
                    userId: testUserRes._id,
                    name: "test",
                };

                const res = await requester
                    .post(`/api/personalList/create`)
                    .set("authorization", jwt.sign({ id: 1 }, "secret"))
                    .send(personalList);

                const content = res.body;
                expect(res).to.have.status(201);
                expect(content).to.have.property("games").and.to.be.empty;
                expect(content).to.have.property("dateCreated");
            });
        });

        describe("updating list", function() {
            it("(PUT /:id/update) should update a lists name", async function() {
                this.timeout(10000);
                const testUser = {
                    username: "test",
                    password: "mock",
                    email: "test@test.com",
                    birthdate: new Date(),
                    role: "user",
                };

                const testUserRes = await new User(testUser).save();

                const personalList = {
                    userId: testUserRes._id,
                    name: "test",
                    games: [],
                    dateCreated: new Date(),
                };

                const personalListNew = {
                    userId: testUserRes._id,
                    name: "newName",
                    games: [],
                    dateCreated: new Date(),
                };

                const personalListRes = await new PersonalList(personalList).save();

                const res = await requester
                    .put(`/api/personalList/${personalListRes._id}/update`)
                    .set("authorization", jwt.sign({ id: 1 }, "secret"))
                    .send(personalListNew);

                expect(res).to.have.status(204);

                const listRes = await requester
                    .get(`/api/personalList/one/${personalListRes._id}`)
                    .set("authorization", jwt.sign({ id: 1 }, "secret"));

                const content = listRes.body;
                expect(listRes).to.have.status(200);
                expect(content).to.have.property("userId", testUserRes._id.toString());
                expect(content).to.have.property("games").and.to.be.empty;
                expect(content).to.have.property("name", "newName");
            });

            it("(PUT /:id/add/game/:gameId) should add one game to a list", async function() {
                this.timeout(10000);
                const testUser = {
                    username: "test",
                    password: "mock",
                    email: "test@test.com",
                    birthdate: new Date(),
                    role: "user",
                };

                const testUserRes = await new User(testUser).save();

                const personalList = {
                    userId: testUserRes._id,
                    name: "test",
                    games: [],
                    dateCreated: new Date(),
                };

                const personalListRes = await new PersonalList(personalList).save();

                const testgame1 = {
                    title: "test",
                    publisher: {
                        name: "test",
                        founder: "test",
                        headquarterAdres: "Test 21",
                    },
                    description: "test",
                    releaseYear: 2010,
                    characters: [],
                    creatorId: "",
                };
                const game = await new Game(testgame1).save();

                const res = await requester
                    .put(`/api/personalList/${personalListRes._id}/add/game/${game._id}`)
                    .set("authorization", jwt.sign({ id: 1 }, "secret"));

                expect(res).to.have.status(200);

                const listRes = await requester
                    .get(`/api/personalList/one/${personalListRes._id}`)
                    .set("authorization", jwt.sign({ id: 1 }, "secret"));

                const content = listRes.body;
                expect(listRes).to.have.status(200);
                expect(content).to.have.property("userId", testUserRes._id.toString());
                expect(content).to.have.property("games").and.not.to.be.empty;
                expect(content).to.have.property("name", "test");
                expect(content.games).to.have.length(1);
                expect(content.games[0]).to.have.property("title", "test");
            });
        });

        describe("deleting list", function() {
            it("(DELETE /:id) should delete a personalList", async function() {
                const personalList = {
                    userId: "60aa5c9820578648ecbd2037",
                    name: "test",
                    games: [],
                    dateCreated: new Date(),
                };

                const personalListRes = await new PersonalList(personalList).save();

                const res = await requester
                    .delete(`/api/personalList/${personalListRes._id}`)
                    .set("authorization", jwt.sign({ id: 1 }, "secret"));

                expect(res).to.have.status(200);
            });

            it("(DELETE /:id/delete/game/:gameId should delete a game out of a list", async function() {
                this.timeout(10000);
                const testUser = {
                    username: "test",
                    password: "mock",
                    email: "test@test.com",
                    birthdate: new Date(),
                    role: "user",
                };

                const testUserRes = await new User(testUser).save();

                const testgame1 = {
                    title: "test",
                    publisher: {
                        name: "test",
                        founder: "test",
                        headquarterAdres: "Test 21",
                    },
                    description: "test",
                    releaseYear: 2010,
                    characters: [],
                    creatorId: "",
                };
                const game = await new Game(testgame1).save();

                const personalList = {
                    userId: testUserRes._id,
                    name: "test",
                    games: [game._id],
                    dateCreated: new Date(),
                };

                const personalListRes = await new PersonalList(personalList).save();

                const res = await requester
                    .delete(
                        `/api/personalList/${personalListRes._id}/delete/game/${game._id}`
                    )
                    .set("authorization", jwt.sign({ id: 1 }, "secret"));

                expect(res).to.have.status(204);

                const listRes = await requester
                    .get(`/api/personalList/one/${personalListRes._id}`)
                    .set("authorization", jwt.sign({ id: 1 }, "secret"));

                const content = await listRes.body;
                expect(listRes).to.have.status(200);
                expect(content).to.have.property("userId", testUserRes._id.toString());
                expect(content).to.have.property("games").and.to.be.empty;
                expect(content).to.have.property("name", "test");
                expect(content.games).to.have.length(0);
            });
        });
    });
});