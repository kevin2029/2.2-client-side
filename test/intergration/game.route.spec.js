require("dotenv").config();

const chai = require("chai");
const expect = chai.expect;

const jwt = require("jsonwebtoken");
const { now } = require("mongoose");

const requester = require("../requester.spec");

const Game = require("../../src/models/game")();
const Character = require("../../src/models/character")();
const PersonalList = require("../../src/models/personalList")();

describe("Game endpoints", function() {
    describe("integration tests", function() {
        describe("getting games", function() {
            it("(GET /) should return all the games", async function() {
                const testgame1 = {
                    title: "test",
                    publisher: {
                        name: "test",
                        founder: "test",
                        headquarterAdres: "Test 21",
                    },
                    description: "test",
                    releaseYear: 2010,
                    characters: [],
                    creatorId: "",
                };

                const testgame2 = {
                    title: "test2",
                    publisher: {
                        name: "test",
                        founder: "test",
                        headquarterAdres: "Test 21",
                    },
                    description: "test2",
                    releaseYear: 2020,
                    characters: [],
                    creatorId: "",
                };

                await new Game(testgame1).save();
                await new Game(testgame2).save();

                const res = await requester.get("/api/game");
                const games = res.body;

                expect(res).to.have.status(200);
                expect(games).to.have.length(2);
                expect(games[0]).to.have.property("title", "test");
                expect(games[0]).to.have.property("characters").and.to.be.empty;
            });

            it("(GET /:id) should return one the game", async function() {
                const testgame1 = {
                    title: "testOne",
                    publisher: {
                        name: "test",
                        founder: "test",
                        headquarterAdres: "Test 21",
                    },
                    description: "test",
                    releaseYear: 2010,
                    characters: [],
                    creatorId: "",
                };

                const testGame = await new Game(testgame1).save();

                const res = await requester.get(`/api/game/${testGame._id}`);
                const game = res.body;

                expect(res).to.have.status(200);
                expect(game).to.have.property("title", "testOne");
                expect(game).to.have.property("characters").and.to.be.empty;
                expect(game).to.have.property("releaseYear", 2010);
            });

            it("(GET /detail/:id) should return a game with loaded characters", async function() {
                const testCharacter = {
                    name: "testName",
                    firstAppearance: "In this test",
                    description: "in testing",
                    gender: "Other",
                };

                const resCharacter = await new Character(testCharacter).save();

                const testgame1 = {
                    title: "testOne",
                    publisher: {
                        name: "test",
                        founder: "test",
                        headquarterAdres: "Test 21",
                    },
                    description: "test",
                    releaseYear: 2010,
                    characters: [resCharacter._id],
                    creatorId: "",
                };

                const testGame = await new Game(testgame1).save();

                const res = await requester.get(`/api/game/detail/${testGame._id}`);
                const game = res.body;

                expect(res).to.have.status(200);
                expect(game).to.have.property("title", "testOne");
                expect(game).to.have.property("characters").and.not.to.be.empty;
                expect(game.characters).to.have.length(1);
                expect(game.characters[0]).to.have.property(
                    "firstAppearance",
                    "In this test"
                );
            });
        });
        describe("creating games", function() {
            it("(POST /create) should create a game", async function() {
                const testgame1 = {
                    title: "test",
                    publisher: {
                        name: "test",
                        founder: "test",
                        headquarterAdres: "Test 21",
                    },
                    description: "test",
                    releaseYear: 2010,
                    characters: [],
                    creatorId: "",
                };

                const res = await requester
                    .post("/api/game/create")
                    .set("authorization", jwt.sign({ id: 1 }, "secret"))
                    .send(testgame1);

                const game = await Game.findById(res.body.id);

                expect(res).to.have.status(201);
                expect(res).to.be.an("object");
                expect(game).to.have.property("title", "test");
                expect(game).to.have.property("releaseYear", 2010);
            });

            it("(POST /create) should show error when field is missing", async function() {
                const testgame = {
                    publisher: {
                        name: "test",
                        founder: "test",
                        headquarterAdres: "Test 21",
                    },
                    description: "test",
                    releaseYear: 2010,
                    characters: [],
                    creatorId: "",
                };

                const res = await requester
                    .post("/api/game/create")
                    .set("authorization", jwt.sign({ id: 1 }, "secret"))
                    .send(testgame);

                expect(res).to.have.status(400);
            });
        });

        describe("update games", function() {
            it("(PUT /update/:id) should update one game", async function() {
                const testgame1 = {
                    title: "test",
                    publisher: {
                        name: "test",
                        founder: "test",
                        headquarterAdres: "Test 21",
                    },
                    description: "test",
                    releaseYear: 2010,
                    characters: [],
                    creatorId: "",
                };

                const game = await new Game(testgame1).save();

                const updategame = {
                    title: "updatedName",
                    publisher: {
                        name: "test",
                        founder: "test",
                        headquarterAdres: "Test 21",
                    },
                    description: "missing",
                    releaseYear: 2010,
                    characters: [],
                    creatorId: "",
                };

                const res = await requester
                    .put(`/api/game/update/${game._id}`)
                    .set("authorization", jwt.sign({ id: 1 }, "secret"))
                    .send(updategame);

                expect(res).to.have.status(204);

                const updatedGame = await requester.get(`/api/game/detail/${game._id}`);
                expect(updatedGame.body).to.have.property("title", "updatedName");
                expect(updatedGame.body).to.have.property("description", "missing");
            });

            it("(PUT /:id/add/character/:characterId) should add one character to one game", async function() {
                const testgame1 = {
                    title: "test",
                    publisher: {
                        name: "test",
                        founder: "test",
                        headquarterAdres: "Test 21",
                    },
                    description: "test",
                    releaseYear: 2010,
                    characters: [],
                    creatorId: "",
                };

                const testCharacter = {
                    name: "testName",
                    firstAppearance: "In this test",
                    description: "in testing",
                    gender: "Other",
                };

                const game = await new Game(testgame1).save();
                const character = await new Character(testCharacter).save();

                const res = await requester
                    .put(`/api/game/${game._id}/add/character/${character._id}`)
                    .set("authorization", jwt.sign({ id: 1 }, "secret"));

                expect(res).to.have.status(200);

                const updatedGame = await requester.get(`/api/game/detail/${game._id}`);
                expect(updatedGame.body).to.have.property("title", "test");
                expect(updatedGame.body).to.have.property("description", "test");

                console.log(updatedGame.body);

                expect(updatedGame.body).to.have.property("characters").and.not.to.be
                    .empty;
                expect(updatedGame.body.characters).to.have.length(1);
                expect(updatedGame.body.characters[0]).to.have.property(
                    "firstAppearance",
                    "In this test"
                );
            });
        });

        describe("deleting games", function() {
            it("(DELETE /api/game/:id) should delete one game", async function() {
                this.timeout(10000);
                const testgame1 = {
                    title: "test",
                    publisher: {
                        name: "test",
                        founder: "test",
                        headquarterAdres: "Test 21",
                    },
                    description: "test",
                    releaseYear: 2010,
                    characters: [],
                    creatorId: "",
                };
                const game = await new Game(testgame1).save();

                const personalList = {
                    userId: "60a93b1b7c9e655590fe9090",
                    name: "test",
                    games: [game._id],
                    dateCreated: new Date(now),
                };

                const personalListObj = await new PersonalList(personalList).save();

                const res = await requester
                    .delete(`/api/game/${game._id}`)
                    .set("authorization", jwt.sign({ id: 1 }, "secret"));

                expect(res).to.have.status(200);

                const listRes = await requester
                    .get(`/api/personalList/one/${personalListObj._id}`)
                    .set("authorization", jwt.sign({ id: 1 }, "secret"));

                expect(listRes.body).to.have.property("games").and.to.be.empty;
                expect(listRes.body.games).to.have.length(0);
            });

            it("(DELETE /api/game/:id/delete/character/:characterId should delete a character out of a game", async function() {
                this.timeout(10000);
                const testCharacter = {
                    name: "testName",
                    firstAppearance: "In this test",
                    description: "in testing",
                    gender: "Other",
                };

                const resCharacter = await new Character(testCharacter).save();

                const testgame = {
                    title: "test",
                    publisher: {
                        name: "test",
                        founder: "test",
                        headquarterAdres: "Test 21",
                    },
                    description: "test",
                    releaseYear: 2010,
                    characters: [resCharacter._id],
                    creatorId: "",
                };

                const game = await new Game(testgame).save();

                const res = await requester
                    .delete(`/api/game/${game._id}/delete/character/${resCharacter._id}`)
                    .set("authorization", jwt.sign({ id: 1 }, "secret"));

                expect(res).to.have.status(204);

                const gameRes = await requester.get(`/api/game/detail/${game._id}`);

                expect(gameRes).to.have.status(200);
                expect(gameRes.body).to.have.property("title", "test");
            });
        });
    });
});