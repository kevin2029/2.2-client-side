require("dotenv").config();

const chai = require("chai");
const expect = chai.expect;

const requester = require("../requester.spec");
const jwt = require("jsonwebtoken");

const User = require("../../src/models/authentication/user")();

describe("Subscribe endpoints", function() {
    describe("integration tests", function() {
        describe("getting subscriptions", function() {
            it("(GET /:userId) should return all the subscriptions for one user", async function() {
                this.timeout(10000);
                await createSubscription();

                const user = await User.findOne({
                    username: "TestUser",
                });

                const res = await requester
                    .get(`/api/subscription/${user._id}`)
                    .set("authorization", jwt.sign({ id: 1 }, "secret"));

                expect(res).to.have.status(200);
                expect(res.body).to.be.an("array");
                expect(res.body[0].username).to.equal("TestUser1");
            });

            it("(GET /:userId/:type) should return all the subscriptions to the user", async function() {
                this.timeout(10000);
                await createSubscription();

                const user = await User.findOne({
                    username: "TestUser1",
                });

                const res = await requester
                    .get(`/api/subscription/${user._id}/myself`)
                    .set("authorization", jwt.sign({ id: 1 }, "secret"));

                expect(res).to.have.status(200);
                expect(res.body).to.be.an("array");

                console.log(res.body);
                expect(res.body[0].username).to.equal("TestUser");
            });
        });

        describe("Making a subscription", function() {
            it("(POST /subscribe) should subscribe a user", async function() {
                this.timeout(10000);
                await createUser();

                const user = await User.findOne({
                    username: "TestUser",
                });

                const user1 = await User.findOne({
                    username: "TestUser1",
                });

                const res = await requester
                    .post(`/api/subscription/subscribe`)
                    .set("authorization", jwt.sign({ id: 1 }, "secret"))
                    .send({
                        userId: user._id,
                        otherUserId: user1._id,
                    });

                expect(res).to.have.status(200);

                const subRes = await requester
                    .get(`/api/subscription/${user._id}`)
                    .set("authorization", jwt.sign({ id: 1 }, "secret"));

                expect(subRes).to.have.status(200);
                expect(subRes.body).to.be.an("array");
                expect(subRes.body[0].username).to.equal("TestUser1");
            });

            it("(POST /subscribe) should not subscribe a user if it already has a subscription", async function() {
                this.timeout(10000);
                await createSubscription();

                const user = await User.findOne({
                    username: "TestUser",
                });

                const user1 = await User.findOne({
                    username: "TestUser1",
                });

                const res = await requester
                    .post(`/api/subscription/subscribe`)
                    .set("authorization", jwt.sign({ id: 1 }, "secret"))
                    .send({
                        userId: user._id,
                        otherUserId: user1._id,
                    });

                expect(res).to.have.status(400);
                expect(res.body).to.have.property(
                    "message",
                    "U bent al geabboneerd op deze persoon."
                );
            });
        });

        describe("Canceling a subscription", function() {
            it("(DELETE /unsubscribe) should delete a subscription", async function() {
                this.timeout(10000);
                await createSubscription();
                const user = await User.findOne({
                    username: "TestUser",
                });

                const user1 = await User.findOne({
                    username: "TestUser1",
                });

                const res = await requester
                    .delete(`/api/subscription/unSubscribe`)
                    .set("authorization", jwt.sign({ id: 1 }, "secret"))
                    .send({
                        userId: user._id,
                        otherUserId: user1._id,
                    });

                expect(res).to.have.status(200);

                const subRes = await requester
                    .get(`/api/subscription/${user._id}`)
                    .set("authorization", jwt.sign({ id: 1 }, "secret"));

                expect(subRes).to.have.status(200);
                expect(subRes.body).to.be.an("array").and.to.be.empty;
            });

            it("(DELETE /unsubscribe) should not delete a subscription when there is no subscription", async function() {
                this.timeout(10000);
                await createUser();
                const user = await User.findOne({
                    username: "TestUser",
                });

                const user1 = await User.findOne({
                    username: "TestUser1",
                });

                const res = await requester
                    .delete(`/api/subscription/unSubscribe`)
                    .set("authorization", jwt.sign({ id: 1 }, "secret"))
                    .send({
                        userId: user._id,
                        otherUserId: user1._id,
                    });

                expect(res).to.have.status(400);
                expect(res.body).to.have.property(
                    "message",
                    "U bent niet geabboneerd op deze persoon."
                );
            });
        });
    });
});

async function createUser() {
    const user = {
        username: "TestUser",
        password: "Test123",
        email: "Test@test.com",
        birthdate: new Date(),
    };

    const user1 = {
        username: "TestUser1",
        password: "Test123",
        email: "Test1@test.com",
        birthdate: new Date(),
    };

    await requester.post("/api/authentication/register").send(user);
    await requester.post("/api/authentication/register").send(user1);
}

async function createSubscription() {
    await createUser();
    const user = await User.findOne({
        username: "TestUser",
    });
    const user1 = await User.findOne({
        username: "TestUser1",
    });

    await requester
        .post("/api/subscription/subscribe")
        .set("authorization", jwt.sign({ id: 1 }, "secret"))
        .send({
            userId: user._id,
            otherUserId: user1._id,
        });
}