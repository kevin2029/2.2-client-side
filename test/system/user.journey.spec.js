require("dotenv").config();

const chai = require("chai");
const expect = chai.expect;

const jwt = require("jsonwebtoken");
const { now } = require("mongoose");

const requester = require("../requester.spec");

const User = require("../../src/models/authentication/user")();
const Game = require("../../src/models/game")();
const Character = require("../../src/models/character")();
const PersonalList = require("../../src/models/personalList")();

describe("user journey", function() {
    it("create game -> create character -> add character to game -> create user -> user makes personalList -> user adds game to list", async function() {
        let res;
        //create game
        const testgame1 = {
            title: "test",
            publisher: {
                name: "test",
                founder: "test",
                headquarterAdres: "Test 21",
            },
            description: "test",
            releaseYear: 2010,
            characters: [],
            creatorId: "",
        };

        res = await requester
            .post("/api/game/create")
            .set("authorization", jwt.sign({ id: 1 }, "secret"))
            .send(testgame1);

        const game = await Game.findById(res.body.id);

        expect(res).to.have.status(201);
        expect(game).to.have.property("title", "test");

        //create character
        const testCharacter = {
            name: "FirstTest",
            firstAppearance: "In this test",
            description: "in testing",
            gender: "Other",
        };

        res = await requester
            .post("/api/character/create")
            .set("authorization", jwt.sign({ id: 1 }, "secret"))
            .send(testCharacter);

        const character = await Character.findById(res.body.id);

        expect(res).to.have.status(201);
        expect(character).to.have.property("firstAppearance", "In this test");

        //add character to game
        res = await requester
            .put(`/api/game/${game._id}/add/character/${character._id}`)
            .set("authorization", jwt.sign({ id: 1 }, "secret"));

        expect(res).to.have.status(200);

        //create user
        const testUser = {
            username: "test",
            password: "mock",
            email: "test@test.com",
            birthdate: new Date(),
            role: "user",
        };

        res = await requester.post("/api/authentication/register").send(testUser);
        expect(res).to.have.status(200);
        const newUser = await User.findOne({ username: "test" });

        //user makes personallist
        const personalList = {
            userId: newUser._id,
            name: "test",
        };

        res = await requester
            .post(`/api/personalList/create`)
            .set("authorization", jwt.sign({ id: 1 }, "secret"))
            .send(personalList);

        const content = res.body;
        expect(res).to.have.status(201);
        expect(content).to.have.property("games").and.to.be.empty;
        expect(content).to.have.property("dateCreated");

        //adds game to list
        res = await requester
            .put(`/api/personalList/${content._id}/add/game/${game._id}`)
            .set("authorization", jwt.sign({ id: 1 }, "secret"));

        expect(res).to.have.status(200);

        const listRes = await requester
            .get(`/api/personalList/one/${content._id}`)
            .set("authorization", jwt.sign({ id: 1 }, "secret"));

        const content1 = listRes.body;

        expect(listRes).to.have.status(200);
        expect(content1).to.have.property("userId", newUser._id.toString());
    });
});