const chai = require("chai");
const expect = chai.expect;

var chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);

const User = require("../../src/models/authentication/user")();

describe("User Model", function() {
    describe("Unit Test", function() {
        it("User should not create when username is missing", async function() {
            const testUser = {
                password: "mock",
                email: "test@test.com",
                birthdate: new Date(),
                role: "user",
            };

            await expect(new User(testUser).validate()).to.be.rejectedWith(
                Error,
                "user validation failed: username: Een gebruiker moet een username hebben."
            );
        });

        it("User should not create when username is is taken", async function() {
            const testUser = {
                username: "test",
                password: "mock",
                email: "test@test.com",
                birthdate: new Date(),
                role: "user",
            };

            const testUser1 = {
                username: "test",
                password: "mock",
                email: "test@test.com",
                birthdate: new Date(),
                role: "user",
            };

            const user = new User(testUser1);

            await new User(testUser).save();

            await expect(user.save()).to.be.rejectedWith(
                Error,
                `E11000 duplicate key error collection: GameDBTest.users index: username_1 dup key: { username: "test" }`
            );
        });

        it("User should not create when password is missing", async function() {
            const testUser = {
                username: "test",

                email: "test@test.com",
                birthdate: new Date(),
                role: "user",
            };

            await expect(new User(testUser).validate()).to.be.rejectedWith(
                Error,
                "user validation failed: password: Een gebruiker moet een password hebben."
            );
        });

        it("User should not create when email is missing", async function() {
            const testUser = {
                username: "test",
                password: "mock",

                birthdate: new Date(),
                role: "user",
            };

            await expect(new User(testUser).validate()).to.be.rejectedWith(
                Error,
                "user validation failed: email: Geef een e-mail adres op."
            );
        });

        it("User should not create when birthdate is missing", async function() {
            const testUser = {
                username: "test",
                password: "mock",
                email: "test@test.com",
                role: "user",
            };

            await expect(new User(testUser).validate()).to.be.rejectedWith(
                Error,
                "user validation failed: birthdate: Een gebruiker moet een geboortedatum hebben."
            );
        });

        it("User should not create when role is missing", async function() {
            const testUser = {
                username: "test",
                password: "mock",
                email: "test@test.com",
                birthdate: new Date(),
            };

            await expect(new User(testUser).validate()).to.be.rejectedWith(
                Error,
                "user validation failed: role: Een gebruiker moet een role hebben."
            );
        });

        it("User should not create when email is not valid", async function() {
            const testUser = {
                username: "test",
                password: "mock",
                email: "testtest.nl",
                birthdate: new Date(),
                role: "user",
            };

            await expect(new User(testUser).validate()).to.be.rejectedWith(
                Error,
                "user validation failed: email: Geef een valide e-mail adres"
            );
        });
    });
});