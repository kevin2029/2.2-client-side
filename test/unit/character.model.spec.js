const chai = require("chai");
const expect = chai.expect;

var chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);

const Character = require("../../src/models/character")();

describe("Character Model", function() {
    describe("Unit Test", function() {
        it("Character should not create when name is missing", async function() {
            const testCharacter = {
                firstAppearance: "In this test",
                description: "in testing",
                gender: "Other",
            };

            await expect(new Character(testCharacter).validate()).to.be.rejectedWith(
                Error,
                "character validation failed: name: Geef de naam op van de character"
            );
        });

        it("Character should not create when firstAppearance is missing", async function() {
            const testCharacter = {
                name: "test",
                description: "in testing",
                gender: "Other",
            };

            await expect(new Character(testCharacter).validate()).to.be.rejectedWith(
                Error,
                "character validation failed: firstAppearance: Geef op bij welke game de character voor het eerst voor kwam."
            );
        });

        it("Character should not create when Gender is missing", async function() {
            const testCharacter = {
                name: "test",
                firstAppearance: "In this test",
                description: "in testing",
            };

            await expect(new Character(testCharacter).validate()).to.be.rejectedWith(
                Error,
                "character validation failed: gender: Geef de sex op"
            );
        });
    });
});