const chai = require("chai");
const expect = chai.expect;

var chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);

const PersonalList = require("../../src/models/personalList")();

describe("PersonalList Model", function() {
    describe("Unit Test", function() {
        it("PersonalList should not create when userID is missing", async function() {
            const personalList = {
                name: "test",
                games: [],
                dateCreated: new Date(),
            };

            await expect(
                new PersonalList(personalList).validate()
            ).to.be.rejectedWith(
                Error,
                "personalList validation failed: userId: Geef UserId mee."
            );
        });
        it("PersonalList should not create when Name is missing", async function() {
            const personalList = {
                userId: "60b39a071e61712a64e34160",
                games: [],
                dateCreated: new Date(),
            };

            await expect(
                new PersonalList(personalList).validate()
            ).to.be.rejectedWith(
                Error,
                "personalList validation failed: name: Geef een naam op"
            );
        });
        it("PersonalList should not create when dateCreated is missing", async function() {
            const personalList = {
                userId: "60b39a071e61712a64e34160",
                name: "test",
                games: [],
            };

            await expect(
                new PersonalList(personalList).validate()
            ).to.be.rejectedWith(
                Error,
                "personalList validation failed: dateCreated: Datum nodig."
            );
        });
        it("PersonalList should create and characters should be empty", async function() {
            const personalList1 = {
                userId: "60b39a071e61712a64e34160",
                name: "test1",
                games: [],
                dateCreated: new Date(),
            };

            const testPersonalList = await new PersonalList(personalList1).save();

            expect(testPersonalList).to.have.property("games").and.to.be.empty;
        });
    });
});