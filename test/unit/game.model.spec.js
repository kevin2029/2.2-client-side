const chai = require("chai");
const expect = chai.expect;

var chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);

const Game = require("../../src/models/game")();

describe("Game Model", function() {
    describe("Unit Test", function() {
        it("game should not create when title is missing", async function() {
            const testgame = {
                publisher: {
                    name: "test",
                    founder: "test",
                    headquarterAdres: "Test 21",
                },
                description: "test",
                releaseYear: 2010,
                characters: [],
                creatorId: "",
            };

            await expect(new Game(testgame).validate()).to.be.rejectedWith(
                Error,
                "game validation failed: title: Geef de titel van de game."
            );
        });

        it("game should not create when releaseYear is missing", async function() {
            const testgame = {
                title: "test",
                publisher: {
                    name: "test",
                    founder: "test",
                    headquarterAdres: "Test 21",
                },
                description: "test",
                characters: [],
                creatorId: "",
            };

            await expect(new Game(testgame).validate()).to.be.rejectedWith(
                Error,
                "game validation failed: releaseYear: Geef het jaar op."
            );
        });

        it("game should not create when publisher is missing", async function() {
            const testgame = {
                title: "test",
                description: "test",
                releaseYear: 2010,
                characters: [],
                creatorId: "",
            };

            await expect(new Game(testgame).validate()).to.be.rejectedWith(
                Error,
                "game validation failed: publisher: Geef informatie over de publisher op."
            );
        });

        it("game should create and characters should be empty", async function() {
            const testgame1 = {
                title: "test",
                publisher: {
                    name: "test",
                    founder: "test",
                    headquarterAdres: "Test 21",
                },
                description: "test",
                releaseYear: 2010,
                characters: [],
            };

            const testGame = await new Game(testgame1).save();

            expect(testGame).to.have.property("characters").and.to.be.empty;
        });
    });
});