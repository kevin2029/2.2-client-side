require("dotenv").config();

const connect = require("../src/connect");
const neo = require("../src/neo");

const Game = require("../src/models/game")();
const Character = require("../src/models/character")();
const PersonalList = require("../src/models/personalList")();
const User = require("../src/models/authentication/user")();

// connect to the databases
connect.mongo(process.env.MONGO_TEST_DB);
connect.neo(process.env.NEO4J_TEST_DB);

beforeEach(async() => {
    // drop both collections before each test
    await Promise.all([
        Game.deleteMany(),
        Character.deleteMany(),
        PersonalList.deleteMany(),
        User.deleteMany(),
    ]);

    //clear neo db before each test
    const session = neo.session();
    await session.run(neo.dropAll);
    await session.close();
});